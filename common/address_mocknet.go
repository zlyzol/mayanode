//go:build testnet || mocknet
// +build testnet mocknet

package common

var (
	RadixAccountAddressPrefix = "account_loc1"
	RadixPrefixAbbreviations  = []string{"loc1", "1"}
)
