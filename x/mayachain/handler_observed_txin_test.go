package mayachain

import (
	"errors"
	"fmt"
	"sort"
	"strings"
	"sync"

	"github.com/blang/semver"
	se "github.com/cosmos/cosmos-sdk/types/errors"
	"github.com/tendermint/tendermint/crypto"
	"github.com/tendermint/tendermint/libs/log"
	. "gopkg.in/check.v1"

	"gitlab.com/mayachain/mayanode/common"
	"gitlab.com/mayachain/mayanode/common/cosmos"
	"gitlab.com/mayachain/mayanode/x/mayachain/keeper"
	"gitlab.com/mayachain/mayanode/x/mayachain/types"
)

type HandlerObservedTxInSuite struct{}

type TestObservedTxInValidateKeeper struct {
	keeper.KVStoreDummy
	activeNodeAccount NodeAccount
	standbyAccount    NodeAccount
}

func (k *TestObservedTxInValidateKeeper) GetNodeAccount(_ cosmos.Context, addr cosmos.AccAddress) (NodeAccount, error) {
	if addr.Equals(k.standbyAccount.NodeAddress) {
		return k.standbyAccount, nil
	}
	if addr.Equals(k.activeNodeAccount.NodeAddress) {
		return k.activeNodeAccount, nil
	}
	return NodeAccount{}, errKaboom
}

func (k *TestObservedTxInValidateKeeper) SetNodeAccount(_ cosmos.Context, na NodeAccount) error {
	if na.NodeAddress.Equals(k.standbyAccount.NodeAddress) {
		k.standbyAccount = na
		return nil
	}
	return errKaboom
}

var _ = Suite(&HandlerObservedTxInSuite{})

func (s *HandlerObservedTxInSuite) TestValidate(c *C) {
	var err error
	ctx, _ := setupKeeperForTest(c)
	activeNodeAccount := GetRandomValidatorNode(NodeActive)
	standbyAccount := GetRandomValidatorNode(NodeStandby)
	keeper := &TestObservedTxInValidateKeeper{
		activeNodeAccount: activeNodeAccount,
		standbyAccount:    standbyAccount,
	}

	handler := NewObservedTxInHandler(NewDummyMgrWithKeeper(keeper))

	// happy path
	pk := GetRandomPubKey()
	txs := ObservedTxs{NewObservedTx(GetRandomTx(), 12, pk, 12)}
	txs[0].Tx.ToAddress, err = pk.GetAddress(txs[0].Tx.Coins[0].Asset.Chain)
	c.Assert(err, IsNil)
	msg := NewMsgObservedTxIn(txs, activeNodeAccount.NodeAddress)
	err = handler.validate(ctx, *msg)
	c.Assert(err, IsNil)

	// inactive node account
	msg = NewMsgObservedTxIn(txs, GetRandomBech32Addr())
	err = handler.validate(ctx, *msg)
	c.Assert(errors.Is(err, se.ErrUnauthorized), Equals, true)

	// invalid msg
	msg = &MsgObservedTxIn{}
	err = handler.validate(ctx, *msg)
	c.Assert(err, NotNil)
}

type TestObservedTxInFailureKeeper struct {
	keeper.KVStoreDummy
	pool Pool
}

func (k *TestObservedTxInFailureKeeper) GetPool(_ cosmos.Context, _ common.Asset) (Pool, error) {
	return k.pool, nil
}

func (s *HandlerObservedTxInSuite) TestFailure(c *C) {
	ctx, _ := setupKeeperForTest(c)
	// w := getHandlerTestWrapper(c, 1, true, false)

	keeper := &TestObservedTxInFailureKeeper{
		pool: Pool{
			Asset:        common.BNBAsset,
			BalanceCacao: cosmos.NewUint(200),
			BalanceAsset: cosmos.NewUint(300),
		},
	}
	mgr := NewDummyMgrWithKeeper(keeper)

	tx := NewObservedTx(GetRandomTx(), 12, GetRandomPubKey(), 12)
	err := refundTx(ctx, tx, mgr, CodeInvalidMemo, "Invalid memo", "")
	c.Assert(err, IsNil)
	items, err := mgr.TxOutStore().GetOutboundItems(ctx)
	c.Assert(err, IsNil)
	c.Check(items, HasLen, 1)
}

type TestObservedTxInHandleKeeper struct {
	keeper.KVStoreDummy
	nas                  NodeAccounts
	voter                ObservedTxVoter
	yggExists            bool
	height               int64
	msg                  MsgSwap
	pool                 Pool
	observing            []cosmos.AccAddress
	vault                Vault
	yggVault             Vault
	txOut                *TxOut
	setLastObserveHeight bool
}

func (k *TestObservedTxInHandleKeeper) SetSwapQueueItem(_ cosmos.Context, msg MsgSwap, _ int) error {
	k.msg = msg
	return nil
}

func (k *TestObservedTxInHandleKeeper) ListActiveValidators(_ cosmos.Context) (NodeAccounts, error) {
	return k.nas, nil
}

func (k *TestObservedTxInHandleKeeper) GetObservedTxInVoter(_ cosmos.Context, _ common.TxID) (ObservedTxVoter, error) {
	return k.voter, nil
}

func (k *TestObservedTxInHandleKeeper) SetObservedTxInVoter(_ cosmos.Context, voter ObservedTxVoter) {
	k.voter = voter
}

func (k *TestObservedTxInHandleKeeper) VaultExists(_ cosmos.Context, _ common.PubKey) bool {
	return k.yggExists
}

func (k *TestObservedTxInHandleKeeper) SetLastChainHeight(_ cosmos.Context, _ common.Chain, height int64) error {
	k.height = height
	return nil
}

func (k *TestObservedTxInHandleKeeper) AddObservingAddresses(_ cosmos.Context, addrs []cosmos.AccAddress) error {
	k.observing = addrs
	return nil
}

func (k *TestObservedTxInHandleKeeper) GetVault(_ cosmos.Context, key common.PubKey) (Vault, error) {
	if k.vault.PubKey.Equals(key) {
		return k.vault, nil
	} else if k.yggVault.PubKey.Equals(key) {
		return k.yggVault, nil
	}
	return GetRandomVault(), errKaboom
}

func (k *TestObservedTxInHandleKeeper) GetAsgardVaults(_ cosmos.Context) (Vaults, error) {
	return Vaults{k.vault}, nil
}

func (k *TestObservedTxInHandleKeeper) SetVault(_ cosmos.Context, vault Vault) error {
	if k.vault.PubKey.Equals(vault.PubKey) {
		k.vault = vault
		return nil
	} else if k.yggVault.PubKey.Equals(vault.PubKey) {
		k.yggVault = vault
	}
	return errKaboom
}

func (k *TestObservedTxInHandleKeeper) GetLowestActiveVersion(_ cosmos.Context) semver.Version {
	return GetCurrentVersion()
}

func (k *TestObservedTxInHandleKeeper) IsActiveObserver(_ cosmos.Context, addr cosmos.AccAddress) bool {
	return addr.Equals(k.nas[0].NodeAddress)
}

func (k *TestObservedTxInHandleKeeper) GetTxOut(ctx cosmos.Context, blockHeight int64) (*TxOut, error) {
	if k.txOut != nil && k.txOut.Height == blockHeight {
		return k.txOut, nil
	}
	return nil, errKaboom
}

func (k *TestObservedTxInHandleKeeper) SetTxOut(ctx cosmos.Context, blockOut *TxOut) error {
	if k.txOut.Height == blockOut.Height {
		k.txOut = blockOut
		return nil
	}
	return errKaboom
}

func (k *TestObservedTxInHandleKeeper) SetLastObserveHeight(ctx cosmos.Context, chain common.Chain, address cosmos.AccAddress, height int64) error {
	k.setLastObserveHeight = true
	return nil
}

func (s *HandlerObservedTxInSuite) TestHandle(c *C) {
	s.testHandleWithVersion(c)
	s.testHandleWithConfirmation(c)
}

func (s *HandlerObservedTxInSuite) testHandleWithConfirmation(c *C) {
	var err error
	ctx, mgr := setupManagerForTest(c)
	tx := GetRandomTx()
	tx.Memo = "SWAP:BTC.BTC:" + GetRandomBTCAddress().String()
	obTx := NewObservedTx(tx, 12, GetRandomPubKey(), 15)
	txs := ObservedTxs{obTx}
	pk := GetRandomPubKey()
	txs[0].Tx.ToAddress, err = pk.GetAddress(txs[0].Tx.Coins[0].Asset.Chain)
	c.Assert(err, IsNil)
	vault := GetRandomVault()
	vault.PubKey = obTx.ObservedPubKey

	keeper := &TestObservedTxInHandleKeeper{
		nas: NodeAccounts{
			GetRandomValidatorNode(NodeActive),
			GetRandomValidatorNode(NodeActive),
			GetRandomValidatorNode(NodeActive),
			GetRandomValidatorNode(NodeActive),
		},
		vault: vault,
		pool: Pool{
			Asset:        common.BNBAsset,
			BalanceCacao: cosmos.NewUint(200),
			BalanceAsset: cosmos.NewUint(300),
		},
		yggExists: true,
	}
	mgr.K = keeper
	handler := NewObservedTxInHandler(mgr)

	// first not confirmed message
	msg := NewMsgObservedTxIn(txs, keeper.nas[0].NodeAddress)
	_, err = handler.handle(ctx, *msg)
	c.Assert(err, IsNil)
	voter, err := keeper.GetObservedTxInVoter(ctx, tx.ID)
	c.Assert(err, IsNil)
	c.Assert(voter.Txs, HasLen, 1)
	// tx has not reach consensus yet, thus fund should not be credit to vault
	c.Assert(keeper.vault.HasFunds(), Equals, false)
	c.Assert(voter.UpdatedVault, Equals, false)
	c.Assert(voter.FinalisedHeight, Equals, int64(0))
	c.Assert(voter.Height, Equals, int64(0))
	mgr.ObMgr().EndBlock(ctx, keeper)

	// second not confirmed message
	msg1 := NewMsgObservedTxIn(txs, keeper.nas[1].NodeAddress)
	_, err = handler.handle(ctx, *msg1)
	c.Assert(err, IsNil)
	voter, err = keeper.GetObservedTxInVoter(ctx, tx.ID)
	c.Assert(err, IsNil)
	c.Assert(voter.Txs, HasLen, 1)
	c.Assert(voter.UpdatedVault, Equals, false)
	c.Assert(voter.FinalisedHeight, Equals, int64(0))
	c.Assert(voter.Height, Equals, int64(0))
	c.Assert(keeper.vault.HasFunds(), Equals, false)

	// third not confirmed message
	msg2 := NewMsgObservedTxIn(txs, keeper.nas[2].NodeAddress)
	_, err = handler.handle(ctx, *msg2)
	c.Assert(err, IsNil)
	voter, err = keeper.GetObservedTxInVoter(ctx, tx.ID)
	c.Assert(err, IsNil)
	c.Assert(voter.Txs, HasLen, 1)
	c.Assert(voter.UpdatedVault, Equals, false)
	c.Assert(voter.FinalisedHeight, Equals, int64(0))
	c.Check(keeper.height, Equals, int64(12))
	// make sure fund has been credit to vault correctly
	bnbCoin := keeper.vault.Coins.GetCoin(common.BNBAsset)
	c.Assert(bnbCoin.Amount.Equal(cosmos.ZeroUint()), Equals, true)
	// make sure the logic has not been processed , as tx has not been finalised , still waiting for confirmation
	c.Check(keeper.msg.Tx.ID.Equals(tx.ID), Equals, false)

	// fourth not confirmed message
	msg3 := NewMsgObservedTxIn(txs, keeper.nas[3].NodeAddress)
	_, err = handler.handle(ctx, *msg3)
	c.Assert(err, IsNil)
	voter, err = keeper.GetObservedTxInVoter(ctx, tx.ID)
	c.Assert(err, IsNil)
	c.Assert(voter.Txs, HasLen, 1)
	c.Assert(voter.UpdatedVault, Equals, false)
	c.Assert(voter.FinalisedHeight, Equals, int64(0))
	c.Check(keeper.height, Equals, int64(12))
	// make sure fund has not been doubled
	bnbCoin = keeper.vault.Coins.GetCoin(common.BNBAsset)
	c.Assert(bnbCoin.Amount.Equal(cosmos.ZeroUint()), Equals, true)
	c.Check(keeper.msg.Tx.ID.Equals(tx.ID), Equals, false)

	//  first finalised message
	txs[0].BlockHeight = 15
	fMsg := NewMsgObservedTxIn(txs, keeper.nas[0].NodeAddress)
	_, err = handler.handle(ctx, *fMsg)
	c.Assert(err, IsNil)
	voter, err = keeper.GetObservedTxInVoter(ctx, tx.ID)
	c.Assert(err, IsNil)
	c.Assert(voter.UpdatedVault, Equals, false)
	c.Assert(voter.FinalisedHeight, Equals, int64(0))
	c.Assert(voter.Height, Equals, int64(18))
	// make sure fund has not been doubled
	bnbCoin = keeper.vault.Coins.GetCoin(common.BNBAsset)
	c.Assert(bnbCoin.Amount.Equal(cosmos.ZeroUint()), Equals, true)
	c.Check(keeper.msg.Tx.ID.Equals(tx.ID), Equals, false)

	// second finalised message
	fMsg1 := NewMsgObservedTxIn(txs, keeper.nas[1].NodeAddress)
	_, err = handler.handle(ctx, *fMsg1)
	c.Assert(err, IsNil)
	voter, err = keeper.GetObservedTxInVoter(ctx, tx.ID)
	c.Assert(err, IsNil)
	c.Assert(voter.UpdatedVault, Equals, false)
	c.Assert(voter.FinalisedHeight, Equals, int64(0))
	c.Assert(voter.Height, Equals, int64(18))
	bnbCoin = keeper.vault.Coins.GetCoin(common.BNBAsset)
	c.Assert(bnbCoin.Amount.Equal(cosmos.ZeroUint()), Equals, true)
	c.Check(keeper.msg.Tx.ID.Equals(tx.ID), Equals, false)

	// third finalised message
	fMsg2 := NewMsgObservedTxIn(txs, keeper.nas[2].NodeAddress)
	_, err = handler.handle(ctx, *fMsg2)
	c.Assert(err, IsNil)
	voter, err = keeper.GetObservedTxInVoter(ctx, tx.ID)
	c.Assert(err, IsNil)
	c.Assert(voter.UpdatedVault, Equals, true)
	c.Assert(voter.FinalisedHeight, Equals, int64(18))
	c.Assert(voter.Height, Equals, int64(18))
	// make sure fund has not been doubled
	bnbCoin = keeper.vault.Coins.GetCoin(common.BNBAsset)
	c.Assert(bnbCoin.Amount.Equal(cosmos.OneUint()), Equals, true)
	c.Check(keeper.msg.Tx.ID.String(), Equals, tx.ID.String())

	// third finalised message
	fMsg3 := NewMsgObservedTxIn(txs, keeper.nas[3].NodeAddress)
	_, err = handler.handle(ctx, *fMsg3)
	c.Assert(err, IsNil)
	voter, err = keeper.GetObservedTxInVoter(ctx, tx.ID)
	c.Assert(err, IsNil)
	c.Assert(voter.UpdatedVault, Equals, true)
	c.Assert(voter.FinalisedHeight, Equals, int64(18))
	c.Assert(voter.Height, Equals, int64(18))
	// make sure fund has not been doubled
	bnbCoin = keeper.vault.Coins.GetCoin(common.BNBAsset)
	c.Assert(bnbCoin.Amount.Equal(cosmos.OneUint()), Equals, true)
	c.Check(keeper.msg.Tx.ID.String(), Equals, tx.ID.String())
}

func (s *HandlerObservedTxInSuite) testHandleWithVersion(c *C) {
	var err error
	ctx, mgr := setupManagerForTest(c)

	tx := GetRandomTx()
	tx.Memo = "SWAP:BTC.BTC:" + GetRandomBTCAddress().String()
	obTx := NewObservedTx(tx, 12, GetRandomPubKey(), 12)
	txs := ObservedTxs{obTx}
	pk := GetRandomPubKey()
	txs[0].Tx.ToAddress, err = pk.GetAddress(txs[0].Tx.Coins[0].Asset.Chain)

	vault := GetRandomVault()
	vault.PubKey = obTx.ObservedPubKey

	keeper := &TestObservedTxInHandleKeeper{
		nas:   NodeAccounts{GetRandomValidatorNode(NodeActive)},
		voter: NewObservedTxVoter(tx.ID, make(ObservedTxs, 0)),
		vault: vault,
		pool: Pool{
			Asset:        common.BNBAsset,
			BalanceCacao: cosmos.NewUint(200),
			BalanceAsset: cosmos.NewUint(300),
		},
		yggExists: true,
	}
	mgr.K = keeper
	handler := NewObservedTxInHandler(mgr)

	c.Assert(err, IsNil)
	msg := NewMsgObservedTxIn(txs, keeper.nas[0].NodeAddress)
	_, err = handler.handle(ctx, *msg)
	c.Assert(err, IsNil)
	mgr.ObMgr().EndBlock(ctx, keeper)
	c.Check(keeper.msg.Tx.ID.Equals(tx.ID), Equals, true)
	c.Check(keeper.observing, HasLen, 1)
	c.Check(keeper.height, Equals, int64(12))
	bnbCoin := keeper.vault.Coins.GetCoin(common.BNBAsset)
	c.Assert(bnbCoin.Amount.Equal(cosmos.OneUint()), Equals, true)
}

// Test migrate memo
func (s *HandlerObservedTxInSuite) TestMigrateMemo(c *C) {
	var err error
	ctx, _ := setupKeeperForTest(c)

	vault := GetRandomVault()
	addr, err := vault.PubKey.GetAddress(common.BNBChain)
	c.Assert(err, IsNil)
	newVault := GetRandomVault()
	txout := NewTxOut(12)
	newVaultAddr, err := newVault.PubKey.GetAddress(common.BNBChain)
	c.Assert(err, IsNil)

	txout.TxArray = append(txout.TxArray, TxOutItem{
		Chain:       common.BNBChain,
		InHash:      common.BlankTxID,
		ToAddress:   newVaultAddr,
		VaultPubKey: vault.PubKey,
		Coin:        common.NewCoin(common.BNBAsset, cosmos.NewUint(1024)),
		Memo:        NewMigrateMemo(1).String(),
	})
	tx := NewObservedTx(common.Tx{
		ID:    GetRandomTxHash(),
		Chain: common.BNBChain,
		Coins: common.Coins{
			common.NewCoin(common.BNBAsset, cosmos.NewUint(1024)),
		},
		Memo:        NewMigrateMemo(12).String(),
		FromAddress: addr,
		ToAddress:   newVaultAddr,
		Gas:         BNBGasFeeSingleton,
	}, 13, vault.PubKey, 13)

	txs := ObservedTxs{tx}
	keeper := &TestObservedTxInHandleKeeper{
		nas:   NodeAccounts{GetRandomValidatorNode(NodeActive)},
		voter: NewObservedTxVoter(tx.Tx.ID, make(ObservedTxs, 0)),
		vault: vault,
		pool: Pool{
			Asset:        common.BNBAsset,
			BalanceCacao: cosmos.NewUint(200),
			BalanceAsset: cosmos.NewUint(300),
		},
		yggExists: true,
		txOut:     txout,
	}

	handler := NewObservedTxInHandler(NewDummyMgrWithKeeper(keeper))

	c.Assert(err, IsNil)
	msg := NewMsgObservedTxIn(txs, keeper.nas[0].NodeAddress)
	_, err = handler.handle(ctx, *msg)
	c.Assert(err, IsNil)
}

type ObservedTxInHandlerTestHelper struct {
	keeper.Keeper
	failListActiveValidators bool
	failVaultExist           bool
	failGetObservedTxInVote  bool
	failGetVault             bool
	failSetVault             bool
}

func NewObservedTxInHandlerTestHelper(k keeper.Keeper) *ObservedTxInHandlerTestHelper {
	return &ObservedTxInHandlerTestHelper{
		Keeper: k,
	}
}

func (h *ObservedTxInHandlerTestHelper) ListActiveValidators(ctx cosmos.Context) (NodeAccounts, error) {
	if h.failListActiveValidators {
		return NodeAccounts{}, errKaboom
	}
	return h.Keeper.ListActiveValidators(ctx)
}

func (h *ObservedTxInHandlerTestHelper) VaultExists(ctx cosmos.Context, pk common.PubKey) bool {
	if h.failVaultExist {
		return false
	}
	return h.Keeper.VaultExists(ctx, pk)
}

func (h *ObservedTxInHandlerTestHelper) GetObservedTxInVoter(ctx cosmos.Context, hash common.TxID) (ObservedTxVoter, error) {
	if h.failGetObservedTxInVote {
		return ObservedTxVoter{}, errKaboom
	}
	return h.Keeper.GetObservedTxInVoter(ctx, hash)
}

func (h *ObservedTxInHandlerTestHelper) GetVault(ctx cosmos.Context, pk common.PubKey) (Vault, error) {
	if h.failGetVault {
		return Vault{}, errKaboom
	}
	return h.Keeper.GetVault(ctx, pk)
}

func (h *ObservedTxInHandlerTestHelper) SetVault(ctx cosmos.Context, vault Vault) error {
	if h.failSetVault {
		return errKaboom
	}
	return h.Keeper.SetVault(ctx, vault)
}

func setupAnLegitObservedTx(ctx cosmos.Context, helper *ObservedTxInHandlerTestHelper, c *C) *MsgObservedTxIn {
	activeNodeAccount := GetRandomValidatorNode(NodeActive)
	pk := GetRandomPubKey()
	tx := GetRandomTx()
	tx.Coins = common.Coins{
		common.NewCoin(common.BNBAsset, cosmos.NewUint(common.One*3)),
	}
	tx.Memo = "SWAP:RUNE"
	addr, err := pk.GetAddress(tx.Coins[0].Asset.Chain)
	c.Assert(err, IsNil)
	tx.ToAddress = addr
	obTx := NewObservedTx(tx, ctx.BlockHeight(), pk, ctx.BlockHeight())
	txs := ObservedTxs{obTx}
	txs[0].Tx.ToAddress, err = pk.GetAddress(txs[0].Tx.Coins[0].Asset.Chain)
	c.Assert(err, IsNil)
	vault := GetRandomVault()
	vault.PubKey = obTx.ObservedPubKey
	c.Assert(helper.Keeper.SetNodeAccount(ctx, activeNodeAccount), IsNil)
	c.Assert(helper.SetVault(ctx, vault), IsNil)
	p := NewPool()
	p.Asset = common.BNBAsset
	p.BalanceCacao = cosmos.NewUint(100 * common.One)
	p.BalanceAsset = cosmos.NewUint(100 * common.One)
	p.Status = PoolAvailable
	c.Assert(helper.Keeper.SetPool(ctx, p), IsNil)
	return NewMsgObservedTxIn(ObservedTxs{
		obTx,
	}, activeNodeAccount.NodeAddress)
}

func (HandlerObservedTxInSuite) TestObservedTxHandler_validations(c *C) {
	testCases := []struct {
		name            string
		messageProvider func(c *C, ctx cosmos.Context, helper *ObservedTxInHandlerTestHelper) cosmos.Msg
		validator       func(c *C, ctx cosmos.Context, result *cosmos.Result, err error, helper *ObservedTxInHandlerTestHelper, name string)
	}{
		{
			name: "invalid message should return an error",
			messageProvider: func(c *C, ctx cosmos.Context, helper *ObservedTxInHandlerTestHelper) cosmos.Msg {
				return NewMsgNetworkFee(ctx.BlockHeight(), common.BNBChain, 1, bnbSingleTxFee.Uint64(), GetRandomBech32Addr())
			},
			validator: func(c *C, ctx cosmos.Context, result *cosmos.Result, err error, helper *ObservedTxInHandlerTestHelper, name string) {
				c.Check(err, NotNil, Commentf(name))
				c.Check(result, IsNil)
				c.Check(errors.Is(err, errInvalidMessage), Equals, true)
			},
		},
		{
			name: "message fail validation should return an error",
			messageProvider: func(c *C, ctx cosmos.Context, helper *ObservedTxInHandlerTestHelper) cosmos.Msg {
				return NewMsgObservedTxIn(ObservedTxs{
					NewObservedTx(GetRandomTx(), ctx.BlockHeight(), GetRandomPubKey(), ctx.BlockHeight()),
				}, GetRandomBech32Addr())
			},
			validator: func(c *C, ctx cosmos.Context, result *cosmos.Result, err error, helper *ObservedTxInHandlerTestHelper, name string) {
				c.Check(err, NotNil, Commentf(name))
				c.Check(result, IsNil)
			},
		},
		{
			name: "signer vote for the same tx should be slashed , and not doing anything else",
			messageProvider: func(c *C, ctx cosmos.Context, helper *ObservedTxInHandlerTestHelper) cosmos.Msg {
				m := setupAnLegitObservedTx(ctx, helper, c)
				voter, err := helper.Keeper.GetObservedTxInVoter(ctx, m.Txs[0].Tx.ID)
				c.Assert(err, IsNil)
				voter.Add(m.Txs[0], m.Signer)
				helper.Keeper.SetObservedTxInVoter(ctx, voter)
				return m
			},
			validator: func(c *C, ctx cosmos.Context, result *cosmos.Result, err error, helper *ObservedTxInHandlerTestHelper, name string) {
				c.Check(err, IsNil, Commentf(name))
				c.Check(result, NotNil, Commentf(name))
			},
		},
		{
			name: "fail to list active node accounts should result in an error",
			messageProvider: func(c *C, ctx cosmos.Context, helper *ObservedTxInHandlerTestHelper) cosmos.Msg {
				m := setupAnLegitObservedTx(ctx, helper, c)
				helper.failListActiveValidators = true
				return m
			},
			validator: func(c *C, ctx cosmos.Context, result *cosmos.Result, err error, helper *ObservedTxInHandlerTestHelper, name string) {
				c.Check(err, NotNil, Commentf(name))
				c.Check(result, IsNil, Commentf(name))
			},
		},
		{
			name: "vault not exist should not result in an error, it should continue",
			messageProvider: func(c *C, ctx cosmos.Context, helper *ObservedTxInHandlerTestHelper) cosmos.Msg {
				m := setupAnLegitObservedTx(ctx, helper, c)
				helper.failVaultExist = true
				return m
			},
			validator: func(c *C, ctx cosmos.Context, result *cosmos.Result, err error, helper *ObservedTxInHandlerTestHelper, name string) {
				c.Check(err, IsNil, Commentf(name))
				c.Check(result, NotNil, Commentf(name))
			},
		},
		{
			name: "fail to get observedTxInVoter should not result in an error, it should continue",
			messageProvider: func(c *C, ctx cosmos.Context, helper *ObservedTxInHandlerTestHelper) cosmos.Msg {
				m := setupAnLegitObservedTx(ctx, helper, c)
				helper.failGetObservedTxInVote = true
				return m
			},
			validator: func(c *C, ctx cosmos.Context, result *cosmos.Result, err error, helper *ObservedTxInHandlerTestHelper, name string) {
				c.Check(err, IsNil, Commentf(name))
				c.Check(result, NotNil, Commentf(name))
			},
		},
		{
			name: "empty memo should not result in an error, it should continue",
			messageProvider: func(c *C, ctx cosmos.Context, helper *ObservedTxInHandlerTestHelper) cosmos.Msg {
				m := setupAnLegitObservedTx(ctx, helper, c)
				m.Txs[0].Tx.Memo = ""
				return m
			},
			validator: func(c *C, ctx cosmos.Context, result *cosmos.Result, err error, helper *ObservedTxInHandlerTestHelper, name string) {
				c.Check(err, IsNil, Commentf(name))
				c.Check(result, NotNil, Commentf(name))
				txOut, err := helper.GetTxOut(ctx, ctx.BlockHeight())
				c.Assert(err, IsNil, Commentf(name))
				c.Assert(txOut.IsEmpty(), Equals, false)
			},
		},
		{
			name: "fail to get vault, it should continue",
			messageProvider: func(c *C, ctx cosmos.Context, helper *ObservedTxInHandlerTestHelper) cosmos.Msg {
				m := setupAnLegitObservedTx(ctx, helper, c)
				helper.failGetVault = true
				return m
			},
			validator: func(c *C, ctx cosmos.Context, result *cosmos.Result, err error, helper *ObservedTxInHandlerTestHelper, name string) {
				c.Check(err, IsNil, Commentf(name))
				c.Check(result, NotNil, Commentf(name))
			},
		},
		{
			name: "fail to set vault, it should continue",
			messageProvider: func(c *C, ctx cosmos.Context, helper *ObservedTxInHandlerTestHelper) cosmos.Msg {
				m := setupAnLegitObservedTx(ctx, helper, c)
				helper.failSetVault = true
				return m
			},
			validator: func(c *C, ctx cosmos.Context, result *cosmos.Result, err error, helper *ObservedTxInHandlerTestHelper, name string) {
				c.Check(err, IsNil, Commentf(name))
				c.Check(result, NotNil, Commentf(name))
			},
		},
		{
			name: "if the vault is not asgard, it should continue",
			messageProvider: func(c *C, ctx cosmos.Context, helper *ObservedTxInHandlerTestHelper) cosmos.Msg {
				m := setupAnLegitObservedTx(ctx, helper, c)
				vault, err := helper.Keeper.GetVault(ctx, m.Txs[0].ObservedPubKey)
				c.Assert(err, IsNil)
				vault.Type = YggdrasilVault
				c.Assert(helper.Keeper.SetVault(ctx, vault), IsNil)
				return m
			},
			validator: func(c *C, ctx cosmos.Context, result *cosmos.Result, err error, helper *ObservedTxInHandlerTestHelper, name string) {
				c.Check(err, IsNil, Commentf(name))
				c.Check(result, NotNil, Commentf(name))
			},
		},
		{
			name: "inactive vault, it should refund",
			messageProvider: func(c *C, ctx cosmos.Context, helper *ObservedTxInHandlerTestHelper) cosmos.Msg {
				m := setupAnLegitObservedTx(ctx, helper, c)
				vault, err := helper.Keeper.GetVault(ctx, m.Txs[0].ObservedPubKey)
				c.Assert(err, IsNil)
				vault.Status = InactiveVault
				c.Assert(helper.Keeper.SetVault(ctx, vault), IsNil)
				return m
			},
			validator: func(c *C, ctx cosmos.Context, result *cosmos.Result, err error, helper *ObservedTxInHandlerTestHelper, name string) {
				c.Check(err, IsNil, Commentf(name))
				c.Check(result, NotNil, Commentf(name))
				txOut, err := helper.GetTxOut(ctx, ctx.BlockHeight())
				c.Assert(err, IsNil, Commentf(name))
				c.Assert(txOut.IsEmpty(), Equals, false)
			},
		},
		{
			name: "chain halt, it should refund",
			messageProvider: func(c *C, ctx cosmos.Context, helper *ObservedTxInHandlerTestHelper) cosmos.Msg {
				m := setupAnLegitObservedTx(ctx, helper, c)
				helper.Keeper.SetMimir(ctx, "HaltTrading", 1)
				return m
			},
			validator: func(c *C, ctx cosmos.Context, result *cosmos.Result, err error, helper *ObservedTxInHandlerTestHelper, name string) {
				c.Check(err, IsNil, Commentf(name))
				c.Check(result, NotNil, Commentf(name))
				txOut, err := helper.GetTxOut(ctx, ctx.BlockHeight())
				c.Assert(err, IsNil, Commentf(name))
				c.Assert(txOut.IsEmpty(), Equals, false)
			},
		},
		{
			name: "normal provision, it should success",
			messageProvider: func(c *C, ctx cosmos.Context, helper *ObservedTxInHandlerTestHelper) cosmos.Msg {
				m := setupAnLegitObservedTx(ctx, helper, c)
				m.Txs[0].Tx.Memo = "add:BNB.BNB"
				return m
			},
			validator: func(c *C, ctx cosmos.Context, result *cosmos.Result, err error, helper *ObservedTxInHandlerTestHelper, name string) {
				c.Check(err, IsNil, Commentf(name))
				c.Check(result, NotNil, Commentf(name))
			},
		},
	}
	versions := []semver.Version{
		GetCurrentVersion(),
	}
	for _, tc := range testCases {
		for _, ver := range versions {
			ctx, mgr := setupManagerForTest(c)
			helper := NewObservedTxInHandlerTestHelper(mgr.Keeper())
			mgr.K = helper
			mgr.currentVersion = ver
			handler := NewObservedTxInHandler(mgr)
			msg := tc.messageProvider(c, ctx, helper)
			result, err := handler.Run(ctx, msg)
			tc.validator(c, ctx, result, err, helper, tc.name)
		}
	}
}

func (s HandlerObservedTxInSuite) TestSwapWithAffiliate(c *C) {
	ctx, mgr := setupManagerForTest(c)

	queue := newSwapQueueVCUR(mgr.Keeper())
	handler := NewObservedTxInHandler(mgr)

	affAddr := GetRandomBaseAddress()
	msg := NewMsgSwap(common.Tx{
		ID:          common.TxID("5E1DF027321F1FE37CA19B9ECB11C2B4ABEC0D8322199D335D9CE4C39F85F115"),
		FromAddress: GetRandomBNBAddress(),
		ToAddress:   GetRandomBNBAddress(),
		Gas:         BNBGasFeeSingleton,
		Chain:       common.BNBChain,
		Coins:       common.Coins{common.NewCoin(common.BNBAsset, cosmos.NewUint(2*common.One))},
		Memo:        "=:ETH.ETH:" + GetRandomETHAddress().String() + "::" + affAddr.String() + ":100",
	}, common.BNBAsset, GetRandomBNBAddress(), cosmos.ZeroUint(), GetRandomBaseAddress(), cosmos.NewUint(100),
		"", "", nil,
		MarketOrder,
		0, 0,
		GetRandomBech32Addr(),
	)
	handler.addSwap(ctx, *msg)
	swaps, err := queue.FetchQueue(ctx, mgr)
	c.Assert(err, IsNil)
	c.Assert(swaps, HasLen, 2)
	c.Check(swaps[0].msg.Tx.Coins[0].Amount.Uint64(), Equals, uint64(198000000))
	c.Check(swaps[1].msg.Tx.Coins[0].Amount.Uint64(), Equals, uint64(2000000))
}

func (s *HandlerObservedTxInSuite) TestVaultStatus(c *C) {
	testCases := []struct {
		name                 string
		statusAtConsensus    VaultStatus
		statusAtFinalisation VaultStatus
	}{
		{
			name:                 "should observe if active on consensus and finalisation",
			statusAtConsensus:    ActiveVault,
			statusAtFinalisation: ActiveVault,
		}, {
			name:                 "should observe if active on consensus, inactive on finalisation",
			statusAtConsensus:    ActiveVault,
			statusAtFinalisation: InactiveVault,
		}, {
			name:                 "should not observe if inactive on consensus",
			statusAtConsensus:    InactiveVault,
			statusAtFinalisation: InactiveVault,
		},
	}
	for _, tc := range testCases {
		var err error
		ctx, mgr := setupManagerForTest(c)
		tx := GetRandomTx()
		tx.Memo = "SWAP:BTC.BTC:" + GetRandomBTCAddress().String()
		obTx := NewObservedTx(tx, 12, GetRandomPubKey(), 15)
		txs := ObservedTxs{obTx}
		vault := GetRandomVault()
		vault.PubKey = obTx.ObservedPubKey
		keeper := &TestObservedTxInHandleKeeper{
			nas:   NodeAccounts{GetRandomValidatorNode(NodeActive)},
			voter: NewObservedTxVoter(tx.ID, make(ObservedTxs, 0)),
			vault: vault,
			pool: Pool{
				Asset:        common.BNBAsset,
				BalanceCacao: cosmos.NewUint(200),
				BalanceAsset: cosmos.NewUint(300),
			},
			yggExists: true,
		}
		mgr.K = keeper
		handler := NewObservedTxInHandler(mgr)

		keeper.vault.Status = tc.statusAtConsensus
		msg := NewMsgObservedTxIn(txs, keeper.nas[0].NodeAddress)
		_, err = handler.handle(ctx, *msg)
		c.Assert(err, IsNil, Commentf(tc.name))
		c.Check(keeper.voter.Height, Equals, int64(18), Commentf(tc.name))

		c.Check(keeper.voter.UpdatedVault, Equals, false, Commentf(tc.name))
		c.Check(keeper.vault.InboundTxCount, Equals, int64(0), Commentf(tc.name))

		keeper.vault.Status = tc.statusAtFinalisation
		txs[0].BlockHeight = 15
		msg = NewMsgObservedTxIn(txs, keeper.nas[0].NodeAddress)
		ctx = ctx.WithBlockHeight(30)
		_, err = handler.handle(ctx, *msg)
		c.Assert(err, IsNil, Commentf(tc.name))
		c.Check(keeper.voter.FinalisedHeight, Equals, int64(30), Commentf(tc.name))

		c.Check(keeper.voter.UpdatedVault, Equals, true, Commentf(tc.name))
		c.Check(keeper.vault.InboundTxCount, Equals, int64(1), Commentf(tc.name))
	}
}

func (s *HandlerObservedTxInSuite) TestYggFundedOnlyFromAsgard(c *C) {
	ctx, mgr := setupManagerForTest(c)

	tx := GetRandomTx()
	vault := GetRandomVault()
	obTx := NewObservedTx(tx, 12, GetRandomPubKey(), 15)

	vault.PubKey = obTx.ObservedPubKey
	vault.Type = AsgardVault
	asgardFromAddr, err := vault.PubKey.GetAddress(common.BNBChain)
	c.Assert(err, IsNil)
	obTx.Tx.FromAddress = asgardFromAddr

	keeper := &TestObservedTxInHandleKeeper{
		nas:       NodeAccounts{GetRandomValidatorNode(NodeActive)},
		voter:     NewObservedTxVoter(tx.ID, make(ObservedTxs, 0)),
		vault:     vault,
		yggExists: true,
	}
	mgr.K = keeper
	handler := NewObservedTxInHandler(mgr)

	// Test isFromAsgard func
	isFromAsgard, err := handler.isFromAsgard(ctx, obTx)
	c.Assert(err, IsNil)
	c.Assert(isFromAsgard, Equals, true)

	obTx.Tx.FromAddress = GetRandomBNBAddress()
	isFromAsgard, err = handler.isFromAsgard(ctx, obTx)
	c.Assert(err, IsNil)
	c.Assert(isFromAsgard, Equals, false)

	// TX is not from Asgard, shouldn't fund Ygg
	fundTx := GetRandomTx()
	fundTx.Memo = "yggdrasil+:15"
	obTx = NewObservedTx(fundTx, 12, GetRandomPubKey(), 15)
	ygg := GetRandomVault()
	ygg.Type = YggdrasilVault
	ygg.PubKey = obTx.ObservedPubKey

	txValue := tx.Coins[0].Amount
	yggBnbBalanceBefore := ygg.GetCoin(common.BNBAsset).Amount

	keeper.yggVault = ygg
	keeper.voter = NewObservedTxVoter(tx.ID, make(ObservedTxs, 0))
	mgr.K = keeper

	handler = NewObservedTxInHandler(mgr)
	txs := ObservedTxs{obTx}
	txs[0].BlockHeight = 15
	msg := NewMsgObservedTxIn(txs, keeper.nas[0].NodeAddress)

	_, err = handler.handle(ctx, *msg)
	c.Assert(err, IsNil)
	c.Assert(keeper.yggVault.GetCoin(common.BNBAsset).Amount.Sub(yggBnbBalanceBefore).Uint64(), Equals, cosmos.ZeroUint().Uint64())

	// TX is from asgard, should fund Ygg
	keeper.voter = NewObservedTxVoter(tx.ID, make(ObservedTxs, 0))
	mgr.K = keeper

	handler = NewObservedTxInHandler(mgr)
	txs[0].Tx.FromAddress = asgardFromAddr
	msg = NewMsgObservedTxIn(txs, keeper.nas[0].NodeAddress)

	_, err = handler.handle(ctx, *msg)
	c.Assert(err, IsNil)
	c.Assert(keeper.yggVault.GetCoin(common.BNBAsset).Amount.Sub(yggBnbBalanceBefore).Uint64(), Equals, txValue.Uint64())
}

type MockWithdrawTxOutStoreForMultiAff struct {
	TxOutStore
	tois   []TxOutItem
	full   bool
	asgard Vault
}

func (store *MockWithdrawTxOutStoreForMultiAff) TryAddTxOutItem(ctx cosmos.Context, mgr Manager, toi TxOutItem, minOut cosmos.Uint) (bool, error) {
	if store.full {
		toi.VaultPubKey = store.asgard.PubKey
		success, err := store.TxOutStore.TryAddTxOutItem(ctx, mgr, toi, minOut)
		if success && err == nil {
			store.tois = append(store.tois, toi)
		}
		return success, err
	} else {
		store.tois = append(store.tois, toi)
		return true, nil
	}
}

type TestAffiliateKeeper struct {
	keeper.Keeper
	nas       NodeAccounts
	voter     ObservedTxVoter
	voterTxID common.TxID
	height    int64
	vault     Vault
	asgard    Vault
}

func (k *TestAffiliateKeeper) ListActiveValidators(_ cosmos.Context) (NodeAccounts, error) {
	return k.nas, nil
}

func (k *TestAffiliateKeeper) GetObservedTxInVoter(_ cosmos.Context, hash common.TxID) (ObservedTxVoter, error) {
	if hash.Equals(k.voter.TxID) {
		return k.voter, nil
	}
	return ObservedTxVoter{TxID: hash}, nil
}

func (k *TestAffiliateKeeper) SetObservedTxInVoter(_ cosmos.Context, voter ObservedTxVoter) {
	k.voter = voter
}

func (k *TestAffiliateKeeper) VaultExists(_ cosmos.Context, key common.PubKey) bool {
	return k.vault.PubKey.Equals(key)
}

func (k *TestAffiliateKeeper) SetLastChainHeight(_ cosmos.Context, _ common.Chain, height int64) error {
	k.height = height
	return nil
}

func (k *TestAffiliateKeeper) GetVault(_ cosmos.Context, key common.PubKey) (Vault, error) {
	if k.vault.PubKey.Equals(key) {
		return k.vault, nil
	}
	if k.asgard.PubKey.Equals(key) {
		return k.asgard, nil
	}
	return GetRandomVault(), errKaboom
}

func (k *TestAffiliateKeeper) SetVault(_ cosmos.Context, vault Vault) error {
	if k.vault.PubKey.Equals(vault.PubKey) {
		k.vault = vault
		return nil
	}
	if k.asgard.PubKey.Equals(vault.PubKey) {
		k.asgard = vault
		return nil
	}
	return errKaboom
}

func (k *TestAffiliateKeeper) GetLowestActiveVersion(_ cosmos.Context) semver.Version {
	return GetCurrentVersion()
}

func (k *TestAffiliateKeeper) IsActiveObserver(_ cosmos.Context, addr cosmos.AccAddress) bool {
	return addr.Equals(k.nas[0].NodeAddress)
}

func (k *TestAffiliateKeeper) GetAsgardVaults(_ cosmos.Context) (Vaults, error) {
	return Vaults{k.asgard}, nil
}

func (k *TestAffiliateKeeper) GetAsgardVaultsByStatus(ctx cosmos.Context, status VaultStatus) (Vaults, error) {
	return Vaults{k.asgard}, nil
}

// ErrorLogCollector is a custom logger writer that filters and collects specific error log messages with the defined substring
// to verify fully ObservedTxInHandler handler tests
type ErrorLogCollector struct {
	mu          sync.Mutex
	substring   string
	checkPrefix bool
	prefix      string
	ignore      []string
	showLogs    bool
	collected   []string
}

// Write processes the log entry and collects error messages ("E") that contain the specified substring
func (mc *ErrorLogCollector) Write(p []byte) (n int, err error) {
	message := string(p)
	if mc.showLogs {
		fmt.Print(message)
	}
	if mc.shouldCollect(message) {
		mc.mu.Lock()
		mc.collected = append(mc.collected, message)
		mc.mu.Unlock()
	}
	return len(p), nil
}

// shouldCollect checks if the message should be collected based on the substring position.
func (mc *ErrorLogCollector) shouldCollect(msg string) bool {
	hasPrefix := mc.checkPrefix && strings.HasPrefix(msg, mc.prefix)
	hasSubstring := len(mc.substring) != 0 && strings.Contains(msg, mc.substring)
	hasIgnore := false
	for _, ignore := range mc.ignore {
		if hasIgnore = strings.Contains(msg, ignore); hasIgnore {
			break
		}
	}
	return (hasPrefix || hasSubstring) && !hasIgnore
}

// GetCollected returns the collected log messages
func (mc *ErrorLogCollector) GetCollected(clear bool) []string {
	mc.mu.Lock()
	defer mc.mu.Unlock()
	c := make([]string, 0, len(mc.collected))
	c = append(c, mc.collected...)
	if clear {
		mc.Clear()
	}
	return c
}

// GetCollectedString returns the collected log messages as one string
func (mc *ErrorLogCollector) GetCollectedString(clear bool) (string, int) {
	s := ""
	logs := mc.GetCollected(clear)
	if len(logs) > 0 {
		s = logs[0]
		for i := 1; i < len(logs); i++ {
			s = s + "\n" + logs[i]
		}
		s = fmt.Sprintf("%d error log occurred: %s", len(logs), s)
	}
	return s, len(logs)
}

func (mc *ErrorLogCollector) Clear() {
	mc.collected = make([]string, 0)
}

// Info implements the log.Info interface, filtering and collecting log messages
func (mc *ErrorLogCollector) Info(msg string, keyvals ...interface{}) {
	if strings.Contains(msg, mc.substring) {
		mc.mu.Lock()
		mc.collected = append(mc.collected, msg)
		mc.mu.Unlock()
	}
}

// Debug implements the log.Debug interface
func (mc *ErrorLogCollector) Debug(msg string, keyvals ...interface{}) {}

// Error implements the log.Error interface
func (mc *ErrorLogCollector) Error(msg string, keyvals ...interface{}) {
	if strings.Contains(msg, mc.substring) {
		mc.mu.Lock()
		mc.collected = append(mc.collected, msg)
		mc.mu.Unlock()
	}
}

// With implements the log.With interface
func (mc *ErrorLogCollector) With(keyvals ...interface{}) log.Logger {
	return mc
}

// NewMessageCollector creates a new MessageCollector.
func NewErrorLogCollector(substring, prefix string, ignore []string, showLogs bool) *ErrorLogCollector {
	e := ErrorLogCollector{
		substring: substring,
		collected: make([]string, 0),
		ignore:    ignore,
		showLogs:  showLogs,
	}
	if prefix != "" {
		e.checkPrefix = true
		e.prefix = prefix
	}
	return &e
}

type MultipleAffiliatesSuite struct {
	ctx             cosmos.Context
	mgr             *Mgrs
	queue           *SwapQueueVCUR
	keeper          *TestAffiliateKeeper
	handler         ObservedTxInHandler
	_depositHandler DepositHandler
	node            types.NodeAccount
	signer          cosmos.AccAddress
	mockTxOutStore  MockWithdrawTxOutStoreForMultiAff
	errLogs         *ErrorLogCollector
	tx              common.Tx
	affMns          []affMn
	affMnMap        map[common.Address]int
	subafAddress    common.Address
}
type affMn struct {
	name   string
	parent string
	bps    cosmos.Uint
	resbps cosmos.Uint
	pref   string
}

var _ = Suite(&MultipleAffiliatesSuite{})

var (
	simple_share_bps = cosmos.NewUint(1_50) // 1.5% share for mayaname 'simple'
	aff_share_bps    = cosmos.NewUint(1_50) // 1.5% share for mayaname 'aff'
)

func (s *MultipleAffiliatesSuite) SetUpTest(c *C) {
	s.ctx, s.mgr = setupManagerForTest(c)

	// Message collector to filter and store error log messages, such as "fail to process inbound tx", eg:
	//     E[2024-08-14|13:47:40.562] fail to process inbound tx                   error="swap destination address is not the same chain as the target asset: unknown request" txhash=79CDAC3BCC22DCFEBCAB5CD50DEF60D83788D3AD0F07BD5ABFAF9B1FF7D350EE
	// if you want show logs in stdout change the last parameter to true
	showLogs := false
	s.errLogs = NewErrorLogCollector("", "E", []string{"\033[43m", "\033[0m", "ignore me", "ignore this error"}, showLogs)
	// Create a logger that uses the custom message collector
	logger := log.NewTMLogger(s.errLogs)
	s.ctx = s.ctx.WithLogger(logger)

	gasFee := s.mgr.gasMgr.GetFee(s.ctx, common.BASEChain, common.BaseAsset())
	gas := common.Gas{common.NewCoin(common.BaseNative, gasFee)}
	from := GetRandomBaseAddress()
	to := GetRandomTHORAddress()

	s.tx = common.NewTx(
		GetRandomTxHash(),
		from, to,
		common.Coins{common.NewCoin(common.BaseAsset(), cosmos.NewUint(10000*common.One))},
		gas, "",
	)

	s.node = GetRandomValidatorNode(NodeActive)
	vault := GetRandomVault()
	vault.PubKey = GetRandomPubKey()
	asgard := GetRandomVault()
	asgard.PubKey = GetRandomPubKey()
	asgard.Coins = common.Coins{
		common.NewCoin(common.BaseNative, cosmos.NewUint(10000000*common.One)),
		common.NewCoin(common.BTCAsset, cosmos.NewUint(10000000*common.One)),
		common.NewCoin(common.BNBAsset, cosmos.NewUint(10000000*common.One)),
		common.NewCoin(common.ETHAsset, cosmos.NewUint(10000000*common.One)),
	}

	s.keeper = &TestAffiliateKeeper{
		nas:       NodeAccounts{s.node},
		voter:     NewObservedTxVoter(s.tx.ID, make(ObservedTxs, 0)),
		voterTxID: s.tx.ID,
		vault:     vault,
		asgard:    asgard,
	}
	s.keeper.Keeper = s.mgr.K
	s.mgr.K = s.keeper

	// mint synths
	coin := common.NewCoin(common.BTCAsset.GetSyntheticAsset(), cosmos.NewUint(100000*common.One))
	c.Assert(s.mgr.Keeper().MintToModule(s.ctx, ModuleName, coin), IsNil)
	synthCoin := common.NewCoin(common.BTCAsset.GetSyntheticAsset(), cosmos.NewUint(5000*common.One))
	c.Assert(s.mgr.Keeper().SendFromModuleToModule(s.ctx, ModuleName, AsgardName, common.NewCoins(synthCoin)), IsNil)

	networkFee := NewNetworkFee(common.THORChain, 1, 2000000)
	c.Assert(s.mgr.Keeper().SaveNetworkFee(s.ctx, common.THORChain, networkFee), IsNil)
	networkFee = NewNetworkFee(common.BTCChain, 70, 500)
	c.Assert(s.mgr.Keeper().SaveNetworkFee(s.ctx, common.BTCChain, networkFee), IsNil)
	networkFee = NewNetworkFee(common.ETHChain, 80000, 300)
	c.Assert(s.mgr.Keeper().SaveNetworkFee(s.ctx, common.ETHChain, networkFee), IsNil)

	txOutStore, err := GetTxOutStore(GetCurrentVersion(), s.mgr.K, s.mgr.eventMgr, s.mgr.gasMgr)
	c.Assert(err, IsNil)
	s.mockTxOutStore = MockWithdrawTxOutStoreForMultiAff{
		TxOutStore: txOutStore,
		asgard:     asgard,
	}
	s.mgr.txOutStore = &s.mockTxOutStore
	s.mockTxOutStore.full = true

	pool := NewPool()
	pool.Asset = common.BNBAsset
	pool.BalanceCacao = cosmos.NewUint(10000000 * common.One)
	pool.BalanceAsset = cosmos.NewUint(10000000 * common.One)
	pool.LPUnits = cosmos.NewUint(500000000 * common.One)
	pool.Status = PoolAvailable
	c.Assert(s.mgr.Keeper().SetPool(s.ctx, pool), IsNil)
	pool.Asset = common.BTCAsset
	pool.BalanceCacao = cosmos.NewUint(10000000 * common.One)
	pool.BalanceAsset = cosmos.NewUint(10000000 * common.One)
	pool.LPUnits = cosmos.NewUint(500000000 * common.One)
	c.Assert(s.mgr.Keeper().SetPool(s.ctx, pool), IsNil)
	pool.Asset = common.RUNEAsset
	pool.BalanceCacao = cosmos.NewUint(10000000 * common.One)
	pool.BalanceAsset = cosmos.NewUint(10000000 * common.One)
	pool.LPUnits = cosmos.NewUint(500000000 * common.One)
	c.Assert(s.mgr.Keeper().SetPool(s.ctx, pool), IsNil)
	pool.Asset = common.ETHAsset
	pool.BalanceCacao = cosmos.NewUint(5000000000 * common.One)
	pool.BalanceAsset = cosmos.NewUint(10000000 * common.One)
	pool.LPUnits = cosmos.NewUint(500000000 * common.One)
	c.Assert(s.mgr.Keeper().SetPool(s.ctx, pool), IsNil)

	s.queue = newSwapQueueVCUR(s.mgr.Keeper())
	s.handler = NewObservedTxInHandler(s.mgr)
	s._depositHandler = NewDepositHandler(s.mgr)

	// prepare & fund deposit signer
	s.signer = GetRandomBech32Addr()
	funds, err := common.NewCoin(common.BaseNative, cosmos.NewUint(3000_000*common.One)).Native()
	c.Assert(err, IsNil)
	err = s.mgr.Keeper().AddCoins(s.ctx, s.signer, cosmos.NewCoins(funds))
	c.Assert(err, IsNil)
	funds, err = common.NewCoin(common.BTCAsset.GetSyntheticAsset(), cosmos.NewUint(5000*common.One)).Native()
	c.Assert(err, IsNil)
	err = s.mgr.Keeper().AddCoins(s.ctx, s.signer, cosmos.NewCoins(funds))
	c.Assert(err, IsNil)

	// register affiliate mayaname for simple aff fee testing
	_, err = s.setMayaname("simple", simple_share_bps, "", EmptyBps, "THOR.RUNE")
	c.Assert(err, IsNil)

	s.subafAddress = common.Address("tmaya13wrmhnh2qe98rjse30pl7u6jxszjjwl4fd6gwn")

	// Map of affiliates with the affiliate Bps and aff fee result Bps of the whole swap amount
	// The 'aff' affiliate operates with a 1.5% fee. From this, subaff_1 receives 30% and subaff_2 gets 20%.
	// Additionally, subaff_1 passes 40% of its share to its sub-sub-affiliate, subsubaff_1.
	// Resulting distribution: aff: 0.75%, subaff_1: 0.27% (60% of 30% of 1.5%), subaff_2: 0.3% (20% of 1.5%), subsubaff_1: 0.18% (40% of 30% of 1.5%).
	s.affMns = []affMn{
		{"aff", "", aff_share_bps, cosmos.NewUint(75), "THOR.RUNE"},
		{"subaff_1", "aff", cosmos.NewUint(30_00), cosmos.NewUint(27), "BTC.BTC"},
		{"subaff_2", "aff", cosmos.NewUint(10_00), cosmos.NewUint(15), ""},
		{s.subafAddress.String(), "aff", cosmos.NewUint(10_00), cosmos.NewUint(15), ""},
		{"subsubaff_1", "subaff_1", cosmos.NewUint(40_00), cosmos.NewUint(18), ""},
	}
	s.affMnMap = make(map[common.Address]int)

	for i, d := range s.affMns {
		bps := EmptyBps
		if d.parent == "" {
			bps = d.bps
		}
		var addr common.Address
		if d.name != s.subafAddress.String() {
			addr, err = s.setMayaname(d.name, bps, "", EmptyBps, d.pref)
		} else {
			addr = common.Address(d.name)
		}
		c.Assert(err, IsNil)
		s.affMnMap[addr] = i
		if d.parent != "" {
			parent, err := s.mgr.Keeper().GetMAYAName(s.ctx, d.parent)
			c.Assert(err, IsNil)
			c.Assert(parent.Name == "", Equals, false)
			prevSubAffCnt := len(parent.GetSubaffiliates())
			_, err = s.setMayaname(d.parent, EmptyBps, d.name, d.bps, "")
			c.Assert(err, IsNil)
			parent, err = s.mgr.Keeper().GetMAYAName(s.ctx, d.parent)
			c.Assert(err, IsNil)
			subAffs := parent.GetSubaffiliates()
			c.Assert(subAffs, HasLen, prevSubAffCnt+1)
			c.Assert(subAffs[prevSubAffCnt].Name, Equals, d.name)
			c.Assert(subAffs[prevSubAffCnt].Bps.Equal(d.bps), Equals, true)
		}
	}
}

func (s *MultipleAffiliatesSuite) handleDeposit(msg *MsgDeposit) error {
	_, err := s._depositHandler.handle(s.ctx, *msg)
	if err == nil {
		if str, cnt := s.errLogs.GetCollectedString(true); cnt > 0 {
			err = fmt.Errorf(str)
		}
	} else {
		fmt.Print(err)
	}
	return err
}

func (s *MultipleAffiliatesSuite) processTx() error {
	s.keeper.voter.FinalisedHeight = 0
	height := int64(12)
	s.tx.ID = GetRandomTxHash()
	obTx := NewObservedTx(s.tx, height, s.keeper.vault.PubKey, 15)
	txs := ObservedTxs{obTx}
	txs[0].BlockHeight = 15
	msg := NewMsgObservedTxIn(txs, s.node.NodeAddress)
	s.ctx = s.ctx.WithBlockHeight(16)
	_, err := s.handler.handle(s.ctx, *msg)
	if err == nil {
		if str, cnt := s.errLogs.GetCollectedString(true); cnt > 0 {
			err = fmt.Errorf(str)
		}
	}
	return err
}

func (s *MultipleAffiliatesSuite) fetchQueue() (swapItems, error) {
	s.mockTxOutStore.tois = nil
	swaps, err := s.queue.FetchQueue(s.ctx, s.mgr)
	if err == nil {
		if str, cnt := s.errLogs.GetCollectedString(true); cnt > 0 {
			err = fmt.Errorf(str)
		}
	}
	sort.SliceStable(swaps, func(i, j int) bool {
		return swaps[i].index < swaps[j].index
	})
	return swaps, err
}

func (s *MultipleAffiliatesSuite) setMayanameSimple(name string, affBps cosmos.Uint, subaff string, subaffBps cosmos.Uint, pref string) error {
	_, err := s.setMayaname(name, affBps, subaff, subaffBps, pref)
	return err
}

func (s *MultipleAffiliatesSuite) setMayaname(name string, affBps cosmos.Uint, subaff string, subaffBps cosmos.Uint, pref string) (common.Address, error) {
	// MAYAName memo format 1:  ~:name:chain:address:?owner:?preferredAsset:?expiry:?affbps:?subaff:?subaffbps
	exists := s.mgr.Keeper().MAYANameExists(s.ctx, name)
	// prepare the mayaname owner
	var owner cosmos.AccAddress
	var alias common.Address
	var mn types.MAYAName
	var err error

	if exists {
		// get the existing owner
		mn, err = s.mgr.Keeper().GetMAYAName(s.ctx, name)
		if err != nil {
			return alias, err
		}
		owner = mn.Owner
	} else {
		// get new owner
		owner = GetRandomBech32Addr()
		funds, err := common.NewCoin(common.BaseNative, cosmos.NewUint(3000_000*common.One)).Native()
		if err != nil {
			return alias, err
		}
		if err := s.mgr.Keeper().AddCoins(s.ctx, owner, cosmos.NewCoins(funds)); err != nil {
			return alias, err
		}
	}
	affBpsStr := ""
	if !affBps.Equal(EmptyBps) {
		affBpsStr = affBps.String()
	}
	subaffBpsStr := ""
	if !subaffBps.Equal(EmptyBps) {
		subaffBpsStr = subaffBps.String()
	}

	var msg *types.MsgDeposit
	coins := common.Coins{common.NewCoin(common.BaseNative, cosmos.NewUint(3_000*common.One))}
	if !exists {
		alias = GetRandomBaseAddress()
		// register mayaname
		msg = NewMsgDeposit(coins, fmt.Sprintf("~:%s:MAYA:%s::::%s:%s:%s", name, alias, affBpsStr, subaff, subaffBpsStr), owner)
	} else if subaff != "" || affBpsStr != "" || subaffBpsStr != "" {
		alias = mn.GetAlias(common.BASEChain)
		// update mayaname
		msg = NewMsgDeposit(coins, fmt.Sprintf("~:%s:MAYA:%s::::%s:%s:%s", name, alias, affBpsStr, subaff, subaffBpsStr), owner)
	}
	if msg != nil {
		err := s.handleDeposit(msg)
		if err != nil {
			return alias, err
		}
	}
	prefWithoutAddress := strings.HasPrefix(pref, "-")
	if prefWithoutAddress {
		pref = pref[1:]
	}
	if len(pref) != 0 {
		prefChain := strings.Split(pref, ".")[0]
		var prefAddr common.Address
		if prefWithoutAddress {
			// we don't want to set alias for preferred asset, use base chain
			prefChain = "MAYA"
			prefAddr = GetRandomBaseAddress()
		} else {
			// set the preferred asset and the corresponding alias
			switch pref {
			case "THOR.RUNE":
				prefAddr = GetRandomTHORAddress()
			case "MAYA.CACAO":
				prefAddr = GetRandomBaseAddress()
			case "BNB.BNB":
				prefAddr = GetRandomBNBAddress()
			case "BTC.BTC":
				prefAddr = GetRandomBTCAddress()
			default:
				return alias, fmt.Errorf("can't use %s as preferred asset in this test", pref)
			}
		}
		// for setting preferred asset we have to use long format
		memo := fmt.Sprintf("~:%s:%s:%s::%s", name, prefChain, prefAddr, pref)
		msg = NewMsgDeposit(coins, memo, owner)
		err := s.handleDeposit(msg)
		if err != nil {
			return alias, err
		}
	}
	errLogs := s.errLogs.GetCollected(true)
	if len(errLogs) > 0 {
		return alias, fmt.Errorf("%d error log occurred: %s", len(errLogs), errLogs[0])
	}
	return alias, nil
}

func (s *MultipleAffiliatesSuite) getRandomTTHORAddress() common.Address {
	name := common.RandHexString(10)
	str, _ := common.ConvertAndEncode("tthor", crypto.AddressHash([]byte(name)))
	base, _ := common.NewAddress(str, GetCurrentVersion())
	return base
}

func (s *MultipleAffiliatesSuite) getRandomTMAYAAddress() common.Address {
	name := common.RandHexString(10)
	str, _ := common.ConvertAndEncode("tmaya", crypto.AddressHash([]byte(name)))
	base, _ := common.NewAddress(str, GetCurrentVersion())
	return base
}

func (s *MultipleAffiliatesSuite) clearAffiliateCollector(name string) {
	if !s.mgr.Keeper().MAYANameExists(s.ctx, name) {
		return
	}
	mn, err := s.mgr.Keeper().GetMAYAName(s.ctx, name)
	if err != nil {
		return
	}
	affCol, err := s.mgr.Keeper().GetAffiliateCollector(s.ctx, mn.Owner)
	if err != nil {
		return
	}
	affCol.CacaoAmount = cosmos.ZeroUint()
	s.mgr.Keeper().SetAffiliateCollector(s.ctx, affCol)
}

func (s *MultipleAffiliatesSuite) clearAllAffiliateCollectors() {
	for _, mn := range s.affMns {
		s.clearAffiliateCollector(mn.name)
	}
	s.clearAffiliateCollector("simple")
}

func (s *MultipleAffiliatesSuite) TestMultipleAffiliatesViaMsgDeposit(c *C) {
	ttd := s.getRandomTTHORAddress()
	ttds := ttd.String()

	// *** affiliate swaps from mayachain to tc via MsgDeposit)
	// swap memo format: =:ASSET:DESTADDR:LIM:AFFILIATE_ADDR_OR_MAYANAME:FEE:DEXAggregatorAddr:FinalTokenAddr:MinAmountOut
	perc100 := cosmos.NewUint(10000)
	simple, err := s.mgr.Keeper().GetMAYAName(s.ctx, "simple")
	c.Assert(err, IsNil)
	// small amount not to trigger preferred asset swap
	swapAmt := cosmos.NewUint(10 * common.One)
	coins := common.Coins{common.NewCoin(common.BaseNative, swapAmt)}
	var swaps swapItems
	var affFees cosmos.Uint

	testAff := func(affBps cosmos.Uint, expSwapCnt int) {
		coins[0].Amount = swapAmt
		// swap with 'simple' affiliate
		c.Assert(err, IsNil)
		memo := "=:THOR.RUNE:" + ttds + "::simple"
		if !affBps.IsZero() {
			memo = memo + ":" + affBps.String()
		} else {
			affBps = simple.GetAffiliateBps()
		}
		msg := NewMsgDeposit(coins, memo, s.signer)
		err = s.handleDeposit(msg)
		c.Assert(err, IsNil)
		// verify affiliate fee amount in affiliate collector
		affFees = common.GetSafeShare(affBps, perc100, swapAmt)
		var affCol types.AffiliateFeeCollector
		affCol, err = s.mgr.Keeper().GetAffiliateCollector(s.ctx, simple.Owner)
		c.Assert(err, IsNil)
		c.Check(affCol.CacaoAmount.Equal(affFees), Equals, true)
		// verify swap queue
		swaps, err = s.fetchQueue()
		c.Assert(err, IsNil)
		c.Assert(swaps, HasLen, expSwapCnt)
		c.Check(swaps[0].msg.Tx.Coins[0].Asset, Equals, coins[0].Asset)
		expectedSwapAmt := common.SafeSub(swapAmt, affFees)
		c.Check(swaps[0].msg.Tx.Coins[0].Amount.Equal(expectedSwapAmt), Equals, true)
		err = s.queue.EndBlock(s.ctx, s.mgr)
		c.Assert(err, IsNil)
		// verify out tx
		c.Check(s.mockTxOutStore.tois, HasLen, expSwapCnt)
		c.Check(s.mockTxOutStore.tois[0].ToAddress.Equals(ttd), Equals, true)
		s.clearAllAffiliateCollectors()
	}

	// normal affiliate test
	testAff(cosmos.ZeroUint(), 1)
	s.mockTxOutStore.tois = nil
	// affiliate test with overwritten affiliate bps
	testAff(cosmos.NewUint(500), 1)
	s.mockTxOutStore.tois = nil
	// preferred asset payment test
	prefAddr := s.getRandomTTHORAddress()
	simple, err = s.mgr.Keeper().GetMAYAName(s.ctx, "simple")
	c.Assert(err, IsNil)
	simple.SetAlias(common.THORChain, prefAddr)
	s.mgr.Keeper().SetMAYAName(s.ctx, simple)
	swapAmt = cosmos.NewUint(300000 * common.One)
	testAff(cosmos.ZeroUint(), 2)
	c.Check(swaps[1].msg.Tx.Coins[0].Amount.Equal(affFees), Equals, true)
	c.Check(swaps[1].msg.Tx.Memo, Equals, "MAYA-PREFERRED-ASSET-simple")
	c.Check(s.mockTxOutStore.tois[1].ToAddress.Equals(prefAddr), Equals, true)
	s.mockTxOutStore.tois = nil
	err = s.queue.EndBlock(s.ctx, s.mgr)
	c.Assert(err, IsNil)
	s.mockTxOutStore.tois = nil
	s.clearAllAffiliateCollectors()

	// no preferred asset - payment to native address test
	simple, err = s.mgr.Keeper().GetMAYAName(s.ctx, "simple")
	c.Assert(err, IsNil)
	simple.PreferredAsset = common.EmptyAsset
	s.mgr.Keeper().SetMAYAName(s.ctx, simple)
	addrSimpleOnMaya := simple.GetAlias(common.BASEChain)
	accSimpleOnMaya, _ := addrSimpleOnMaya.AccAddress()
	prevBalance := cosmos.NewUint(s.mgr.Keeper().GetBalance(s.ctx, accSimpleOnMaya).AmountOf(common.BaseAsset().Native()).Uint64())
	swapAmt = cosmos.NewUint(200000 * common.One)
	coins[0].Amount = swapAmt
	memo := "=:THOR.RUNE:" + ttds + "::simple"
	msg := NewMsgDeposit(coins, memo, s.signer)
	err = s.handleDeposit(msg)
	c.Assert(err, IsNil)
	affCol, err := s.mgr.Keeper().GetAffiliateCollector(s.ctx, simple.Owner)
	c.Assert(err, IsNil)
	c.Check(affCol.CacaoAmount.IsZero(), Equals, true)
	// verify swap queue
	swaps, err = s.fetchQueue()
	c.Assert(err, IsNil)
	c.Check(swaps, HasLen, 1) // only the main swap is queued, pref aff swap is not, aff fee sent directly
	s.mockTxOutStore.tois = nil
	affBps := simple.GetAffiliateBps()
	affFees = common.GetSafeShare(affBps, perc100, swapAmt)
	curBalance := cosmos.NewUint(s.mgr.Keeper().GetBalance(s.ctx, accSimpleOnMaya).AmountOf(common.BaseAsset().Native()).Uint64())
	// verify that affiliate fees have been sent to the preferred asset alias on native chain
	c.Assert(prevBalance.Add(affFees).Equal(curBalance), Equals, true)

	// synth swap affiliate
	prevBalance = curBalance
	swapAmt = cosmos.NewUint(500000000000)
	synthCoins := common.Coins{common.NewCoin(common.BTCAsset.GetSyntheticAsset(), swapAmt)}
	memo = "=:BTC.BTC:" + GetRandomBTCAddress().String() + "::simple:150"
	msg = NewMsgDeposit(synthCoins, memo, s.signer)
	err = s.handleDeposit(msg)
	c.Assert(err, IsNil)
	affCol, err = s.mgr.Keeper().GetAffiliateCollector(s.ctx, simple.Owner)
	c.Assert(err, IsNil)
	c.Check(affCol.CacaoAmount.IsZero(), Equals, true)
	// verify swap queue - there should be 2 swaps:
	// 1. main swap btc/btc->eth
	// 2. affiliate swap btc/btc->cacao
	swaps, err = s.fetchQueue()
	c.Assert(err, IsNil)
	c.Check(swaps, HasLen, 2)
	err = s.queue.EndBlock(s.ctx, s.mgr)
	c.Assert(err, IsNil)
	s.mockTxOutStore.tois = nil
	affBps = simple.GetAffiliateBps()
	affFees = common.GetSafeShare(affBps, perc100, swapAmt)
	curBalance = cosmos.NewUint(s.mgr.Keeper().GetBalance(s.ctx, accSimpleOnMaya).AmountOf(common.BaseAsset().Native()).Uint64())
	curBalanceSyntBtc := cosmos.NewUint(s.mgr.Keeper().GetBalance(s.ctx, accSimpleOnMaya).AmountOf(common.BTCAsset.GetSyntheticAsset().Native()).Uint64())
	// verify that affiliate fees have been sent to the preferred asset alias on native chain
	c.Assert(curBalance.GT(prevBalance), Equals, true)
	c.Assert(curBalanceSyntBtc.IsZero(), Equals, true) // no synth sent to affiliate, it has been swapped to cacao

	// Now, here's where the magic happens: a swap with the 'aff' affiliate that includes sub- and sub-sub-affiliates.
	// The 'aff' affiliate operates with a 1.5% fee. From this, subaff_1 receives 30% and subaff_2 gets 20%.
	// Additionally, subaff_1 passes 40% of its share to its sub-sub-affiliate, subsubaff_1.
	// Resulting distribution: aff: 0.75%, subaff_1: 0.27% (60% of 30% of 1.5%), subaff_2: 0.3% (20% of 1.5%), subsubaff_1: 0.18% (40% of 30% of 1.5%).
	s.clearAllAffiliateCollectors()
	// use small amount to swap not to trigger preferred asset swaps so affiliate fees remain in affiliate collector
	swapAmt = cosmos.NewUint(10 * common.One)
	coins[0].Amount = swapAmt
	msg = NewMsgDeposit(coins, "=:THOR.RUNE:"+ttds+"::aff", s.signer)
	err = s.handleDeposit(msg)
	c.Assert(err, IsNil)
	sumFees := cosmos.ZeroUint()
	// verify affiliate fee amount in affiliate collectors of affiliates
	for _, d := range s.affMns {
		affFees = common.GetSafeShare(d.resbps, perc100, swapAmt)
		sumFees = sumFees.Add(affFees)
		if d.name != s.subafAddress.String() {
			mn, err2 := s.mgr.Keeper().GetMAYAName(s.ctx, d.name)
			c.Assert(err2, IsNil)
			if !mn.PreferredAsset.IsEmpty() {
				affCol, err2 := s.mgr.Keeper().GetAffiliateCollector(s.ctx, mn.Owner)
				c.Assert(err2, IsNil)
				cacaoUint := affCol.CacaoAmount.Uint64()
				affFeeUint := affFees.Uint64()
				c.Check(cacaoUint, Equals, affFeeUint)
			}
		}
	}
	// verify swap queue - the first swap is the main swap
	swaps, err = s.fetchQueue()
	c.Assert(err, IsNil)
	c.Assert(swaps, HasLen, 1)
	c.Check(swaps[0].msg.Tx.Coins[0].Asset, Equals, coins[0].Asset)
	expectedSwapAmt := swapAmt.Sub(sumFees)
	gotSwapAmtUint := swaps[0].msg.Tx.Coins[0].Amount.Uint64()
	expectedSwapAmtUint := expectedSwapAmt.Uint64()
	c.Check(gotSwapAmtUint, Equals, expectedSwapAmtUint)
	err = s.queue.EndBlock(s.ctx, s.mgr)
	c.Assert(err, IsNil)
	// verify out tx
	c.Check(s.mockTxOutStore.tois, HasLen, 1)
	c.Check(s.mockTxOutStore.tois[0].ToAddress.Equals(ttd), Equals, true)
	s.mockTxOutStore.tois = nil
	s.clearAllAffiliateCollectors()
}

func (s *MultipleAffiliatesSuite) TestMultipleAffiliatesViaSwaps(c *C) {
	// clear the queue, txouts and affcoll
	_ = s.queue.EndBlock(s.ctx, s.mgr)
	s.mockTxOutStore.tois = nil
	s.clearAllAffiliateCollectors()
	// test via swap to BTC
	s.MultipleAffiliatesViaSwapTo("BTC.BTC", c)
	// clear the queue, txouts and affcoll
	_ = s.queue.EndBlock(s.ctx, s.mgr)
	s.mockTxOutStore.tois = nil
	s.clearAllAffiliateCollectors()
	// test via swap to CACAO
	s.MultipleAffiliatesViaSwapTo("MAYA.CACAO", c)
}

func (s *MultipleAffiliatesSuite) MultipleAffiliatesViaSwapTo(swapTo string, c *C) {
	affColAddress, err := s.mgr.Keeper().GetModuleAddress(AffiliateCollectorName)
	c.Assert(err, IsNil)
	swapTargetAddress := s.getRandomTMAYAAddress()
	if swapTo == "BTC.BTC" {
		swapTargetAddress = GetRandomBTCAddress()
	}
	perc100 := cosmos.NewUint(10000)
	swapAmt := cosmos.NewUint(10000 * common.One)

	// set tx source to BNB
	gasFee := s.mgr.gasMgr.GetFee(s.ctx, common.BNBChain, common.BNBAsset)
	s.tx.Gas = common.Gas{common.NewCoin(common.BNBAsset, gasFee)}
	s.tx.FromAddress = GetRandomBNBAddress()
	s.tx.Coins = common.Coins{common.NewCoin(common.BNBAsset, swapAmt)}

	// swap with 'simple' affiliate
	s.tx.Coins[0].Amount = swapAmt
	simple, err := s.mgr.Keeper().GetMAYAName(s.ctx, "simple")
	c.Assert(err, IsNil)
	s.tx.Memo = fmt.Sprintf("=:%s:%s::simple", swapTo, swapTargetAddress)
	// process the tx
	c.Assert(s.processTx(), IsNil)
	// verify swap queue
	swaps, err := s.fetchQueue()
	c.Assert(err, IsNil)
	c.Assert(swaps, HasLen, 2)
	affFee := common.GetSafeShare(simple_share_bps, perc100, swapAmt)
	expectedSwapAmt := common.SafeSub(swapAmt, affFee)
	c.Check(swaps[0].msg.Tx.Coins[0].Amount.Equal(expectedSwapAmt), Equals, true)
	c.Check(swaps[1].msg.Tx.Coins[0].Amount.Equal(affFee), Equals, true)
	c.Check(swaps[1].msg.Destination.Equals(affColAddress), Equals, true)
	err = s.queue.EndBlock(s.ctx, s.mgr)
	c.Assert(err, IsNil)
	// Verify the affiliate fee amount in the affiliate collector
	affCol, err := s.mgr.Keeper().GetAffiliateCollector(s.ctx, simple.Owner)
	c.Assert(err, IsNil)
	c.Check(affCol.CacaoAmount.GT(cosmos.ZeroUint()), Equals, true)
	// verify out tx
	c.Check(s.mockTxOutStore.tois, HasLen, 2)
	c.Check(s.mockTxOutStore.tois[0].ToAddress.Equals(swapTargetAddress), Equals, true)
	c.Check(s.mockTxOutStore.tois[1].ToAddress.Equals(affColAddress), Equals, true)
	s.mockTxOutStore.tois = nil
	s.clearAllAffiliateCollectors()

	// swap with simple affiliate overwriting default bps
	s.tx.Coins[0].Amount = swapAmt
	s.tx.Memo = fmt.Sprintf("=:%s:%s::simple:500", swapTo, swapTargetAddress)
	// process the tx
	c.Assert(s.processTx(), IsNil)
	swaps, err = s.fetchQueue()
	c.Assert(err, IsNil)
	c.Assert(swaps, HasLen, 2)
	affFee = common.GetSafeShare(cosmos.NewUint(5_00), perc100, swapAmt)
	expectedSwapAmt = common.SafeSub(swapAmt, affFee)
	// verify the main swap destination
	c.Check(swaps[0].msg.Tx.Coins[0].Amount.Equal(expectedSwapAmt), Equals, true)
	c.Check(swaps[0].msg.Destination.Equals(swapTargetAddress), Equals, true)
	// verify the swap of the affiliate fee to the affiliate collector module
	c.Check(swaps[1].msg.Tx.Coins[0].Amount.Equal(affFee), Equals, true)
	c.Check(swaps[1].msg.Destination.Equals(affColAddress), Equals, true)
	err = s.queue.EndBlock(s.ctx, s.mgr)
	c.Assert(err, IsNil)
	// verify that affiliate fee is in the affiliate collector
	affCol, err = s.mgr.Keeper().GetAffiliateCollector(s.ctx, simple.Owner)
	c.Assert(err, IsNil)
	c.Check(affCol.CacaoAmount.GT(cosmos.ZeroUint()), Equals, true)
	// verify out tx
	c.Check(s.mockTxOutStore.tois, HasLen, 2)
	c.Check(s.mockTxOutStore.tois[0].ToAddress.Equals(swapTargetAddress), Equals, true)
	c.Check(s.mockTxOutStore.tois[1].ToAddress.Equals(affColAddress), Equals, true)
	s.mockTxOutStore.tois = nil
	// clear the affCol
	s.clearAllAffiliateCollectors()

	// ******* Trigger preferred asset fee payment from affCol
	// set preferred asset
	s.mockTxOutStore.full = true
	prefAddr := s.getRandomTTHORAddress()
	simple.SetAlias(common.THORChain, prefAddr)
	s.mgr.Keeper().SetMAYAName(s.ctx, simple)

	swapAmt = cosmos.NewUint(300000 * common.One) // bigger amount to trigger the preferred asset payment
	affFee = common.GetSafeShare(simple_share_bps, perc100, swapAmt)
	expectedSwapAmt = common.SafeSub(swapAmt, affFee)
	s.tx.Coins[0].Amount = swapAmt
	s.tx.Memo = fmt.Sprintf("=:%s:%s::simple", swapTo, swapTargetAddress)

	// In the first swap, the preferred asset is not paid out - even if it should be -
	// due to a design flaw in the affiliate collector:
	// It attempts to perform a preferred asset swap before the swap fee is actually swapped to CACAO
	// and added to the Afiliate collector of the affiliate mayaname and transferred to the affiliate collector module
	// The addition and transfer occur only during the processing of TxOut
	// That means triggering preferred asset payout is successful only during the second swap
	// This problem only occurs in the case of a swap from chains other than the native/base chain

	// **** first swap
	// process the tx
	err = s.processTx()
	c.Assert(err, IsNil)
	swaps, err = s.fetchQueue()
	c.Assert(err, IsNil)
	// verify the results
	c.Assert(swaps, HasLen, 2)
	// the main swap
	c.Check(swaps[0].msg.Destination.Equals(swapTargetAddress), Equals, true)
	c.Check(swaps[0].msg.Tx.Coins[0].Amount.Equal(expectedSwapAmt), Equals, true)
	// affiliate fee swap to affiliate collector module
	c.Check(swaps[1].msg.Destination.Equals(affColAddress), Equals, true)
	c.Check(swaps[1].msg.Tx.Coins[0].Amount.Equal(affFee), Equals, true)
	err = s.queue.EndBlock(s.ctx, s.mgr)
	c.Assert(err, IsNil)
	// verify that affCol is empty after preferred asset is paid out
	affCol, err = s.mgr.Keeper().GetAffiliateCollector(s.ctx, simple.Owner)
	c.Assert(err, IsNil)
	c.Check(affCol.CacaoAmount.GT(cosmos.ZeroUint()), Equals, true)
	affColPrevCacaoAmount := affCol.CacaoAmount
	// check out txs
	c.Check(s.mockTxOutStore.tois, HasLen, 2)
	c.Check(s.mockTxOutStore.tois[0].ToAddress.Equals(swapTargetAddress), Equals, true)
	c.Check(s.mockTxOutStore.tois[1].ToAddress.Equals(affColAddress), Equals, true)
	s.mockTxOutStore.tois = nil

	// ******* second swap - this will trigger the preferred asset swap
	// smaller amount is ok, because even a small swap will trigger the accumulated fees from previous swaps to be paid out to preferred asset address
	swapAmt = cosmos.NewUint(10000 * common.One)
	affFee = common.GetSafeShare(simple_share_bps, perc100, swapAmt)
	expectedSwapAmt = common.SafeSub(swapAmt, affFee)
	s.tx.Coins[0].Amount = swapAmt
	err = s.processTx()
	c.Assert(err, IsNil)
	swaps, err = s.fetchQueue()
	c.Assert(err, IsNil)
	// verify the results
	c.Assert(swaps, HasLen, 3)
	// the main swap
	c.Check(swaps[0].msg.Destination.Equals(swapTargetAddress), Equals, true)
	c.Check(swaps[0].msg.Tx.Coins[0].Amount.Equal(expectedSwapAmt), Equals, true)
	// the preferred asset swap from affiliate collector (CACAO) to the preferred asset address (RUNE)
	c.Check(swaps[1].msg.Destination.Equals(prefAddr), Equals, true)
	c.Check(swaps[1].msg.Tx.Memo, Equals, "MAYA-PREFERRED-ASSET-simple")
	// affiliate fee swap to affiliate collector module
	c.Check(swaps[2].msg.Destination.Equals(affColAddress), Equals, true)
	c.Check(swaps[2].msg.Tx.Coins[0].Amount.Equal(affFee), Equals, true)
	err = s.queue.EndBlock(s.ctx, s.mgr)
	c.Assert(err, IsNil)
	// verify that affCol is empty after preferred asset is paid out
	// it should be more than zero, as we swapped a small amount so some small fee has been paid
	// but should be less than the previous bigger amount in affCol
	affCol, err = s.mgr.Keeper().GetAffiliateCollector(s.ctx, simple.Owner)
	c.Check(affCol.CacaoAmount.GT(cosmos.ZeroUint()), Equals, true)
	c.Check(affCol.CacaoAmount.LT(affColPrevCacaoAmount), Equals, true)
	c.Assert(err, IsNil)
	// check out txs
	c.Check(s.mockTxOutStore.tois, HasLen, 3)
	// the triggered preferred asset txout
	c.Check(s.mockTxOutStore.tois[1].ToAddress.Equals(prefAddr), Equals, true)
	// the main swap
	c.Check(s.mockTxOutStore.tois[0].ToAddress.Equals(swapTargetAddress), Equals, true)
	// second affiliate fee swap to affiliate collector module
	c.Check(s.mockTxOutStore.tois[2].ToAddress.Equals(affColAddress), Equals, true)
	s.mockTxOutStore.tois = nil
	s.clearAllAffiliateCollectors()

	findTxOut := func(dest common.Address) int {
		for i := 0; i < len(s.mockTxOutStore.tois); i++ {
			if dest.Equals(s.mockTxOutStore.tois[i].ToAddress) {
				return i
			}
		}
		return -1
	}

	// Now, here's where the magic happens: a swap with the 'aff' affiliate that includes sub- and sub-sub-affiliates.
	// The 'aff' affiliate operates with a 1.5% fee. From this, subaff_1 receives 30% and subaff_2 gets 20%.
	// Additionally, subaff_1 passes 40% of its share to its sub-sub-affiliate, subsubaff_1.
	// Resulting distribution: aff: 0.75%, subaff_1: 0.27% (60% of 30% of 1.5%), subaff_2: 0.3% (20% of 1.5%), subsubaff_1: 0.18% (40% of 30% of 1.5%).
	swapAmt = cosmos.NewUint(30000 * common.One)
	s.tx.Coins[0].Amount = swapAmt
	s.tx.Memo = fmt.Sprintf("=:%s:%s::aff", swapTo, swapTargetAddress)
	c.Assert(s.processTx(), IsNil)
	swaps, err = s.fetchQueue()
	c.Assert(err, IsNil)
	err = s.queue.EndBlock(s.ctx, s.mgr)
	c.Assert(err, IsNil)
	c.Check(swaps, HasLen, 1+len(s.affMns)) // the main swap + the affiliate swaps
	// verify the main swap
	c.Check(swaps[0].msg.Destination.Equals(swapTargetAddress), Equals, true)
	// the main swap amount is 98.5% of the swapped amount (-1.5% fee)
	expectedSwapAmt98_5Uint := swapAmt.MulUint64(985).QuoUint64(1000).Uint64()
	swappedAmountUint := swaps[0].msg.Tx.Coins[0].Amount.Uint64()
	c.Check(swappedAmountUint, Equals, expectedSwapAmt98_5Uint)
	// verify the main swap outtx and the txout to affiliate collector module
	c.Check(s.mockTxOutStore.tois[0].ToAddress.Equals(swapTargetAddress), Equals, true)
	sumInAffColls := cosmos.ZeroUint()
	sumDirectSend := cosmos.ZeroUint()
	sumSentToAffColModule := cosmos.ZeroUint()
	gasCacao := s.mgr.gasMgr.GetFee(s.ctx, common.BASEChain, common.BaseAsset())
	// calculate the target swap full amount
	swappedAmt := s.mockTxOutStore.tois[0].Coin.Amount     // 98.5%
	swappedAmt = swappedAmt.MulUint64(1000).QuoUint64(985) // 100%
	// verify affiliate fee swaps and txouts and affiliate collectors
	for i := 1; i < len(s.affMns)+1; i++ {
		index, found := s.affMnMap[swaps[i].msg.Destination]
		if found { // alias found that means affiliate fee is sent to the alias
			// verify the fee swaps
			affFee := swapAmt.Mul(s.affMns[index].resbps).QuoUint64(10000)
			c.Check(swaps[i].msg.Tx.Coins[0].Amount.Equal(affFee), Equals, true)
			if s.affMns[index].name == s.subafAddress.String() {
				txOutIndex := findTxOut(s.subafAddress)
				if txOutIndex >= 0 {
					outAmt := s.mockTxOutStore.tois[txOutIndex].Coin.Amount
					sumDirectSend = sumDirectSend.Add(outAmt)
				}
			} else {
				mn, err := s.mgr.Keeper().GetMAYAName(s.ctx, s.affMns[index].name)
				c.Assert(err, IsNil)
				alias := mn.GetAlias(common.BASEChain)
				if !alias.IsEmpty() {
					txOutIndex := findTxOut(alias)
					if txOutIndex >= 0 {
						outAmt := s.mockTxOutStore.tois[txOutIndex].Coin.Amount
						sumDirectSend = sumDirectSend.Add(outAmt)
					}
				}
			}
		} else { // msg. Destination not found in aliases, that means fee goes to aff collector
			c.Check(swaps[i].msg.Destination.Equals(affColAddress), Equals, true)
			owner, err := swaps[i].msg.AffiliateAddress.AccAddress()
			c.Assert(err, IsNil)
			// calculate the sum of all affiliate collector amounts
			affCol, err = s.mgr.Keeper().GetAffiliateCollector(s.ctx, owner)
			c.Assert(err, IsNil)
			sumInAffColls = sumInAffColls.Add(affCol.CacaoAmount)
		}
	}

	// calculate the sum of all swaps to affiliate collector module
	for i := 1; i < len(s.affMns)+1; i++ {
		if s.mockTxOutStore.tois[i].ToAddress.Equals(affColAddress) {
			sumSentToAffColModule = sumSentToAffColModule.Add(s.mockTxOutStore.tois[i].Coin.Amount).Sub(gasCacao)
		}
	}
	// the sum of tx out amounts to aff coll module should be the same as the sum of affiliate collectors's amounts
	sumsAreEqual := sumSentToAffColModule.Equal(sumInAffColls)
	c.Check(sumsAreEqual, Equals, true)
	// the sum of affiliate fees should be the 1.5% of the whole swap, minus some gas fees
	// should be more than 1% and less then 1.5%
	sumDirectPlusAffCol := sumDirectSend.Add(sumInAffColls)
	feeBpsOfSwap := sumDirectPlusAffCol.MulUint64(10000).Quo(swappedAmt).Uint64()
	c.Check(feeBpsOfSwap > 1_00 && feeBpsOfSwap < aff_share_bps.Uint64(), Equals, true)

	s.mockTxOutStore.tois = nil
	s.clearAllAffiliateCollectors()
}

func (s *MultipleAffiliatesSuite) TestMultipleAffiliatesErrors(c *C) {
	// Try to register two affiliate, which would result in the total fees exceeding 100%
	c.Assert(s.setMayanameSimple("alfa", EmptyBps, "", EmptyBps, ""), IsNil)
	c.Assert(s.setMayanameSimple("aff1", EmptyBps, "", EmptyBps, ""), IsNil)
	c.Assert(s.setMayanameSimple("aff2", EmptyBps, "", EmptyBps, ""), IsNil)
	c.Assert(s.setMayanameSimple("alfa", EmptyBps, "aff1", cosmos.NewUint(50_00), ""), IsNil)
	c.Assert(s.setMayanameSimple("alfa", EmptyBps, "aff2", cosmos.NewUint(60_00), ""), NotNil)
	// set self as subaffiliate - should fail
	c.Assert(s.setMayanameSimple("hello", EmptyBps, "", EmptyBps, ""), IsNil)
	c.Assert(s.setMayanameSimple("hello", EmptyBps, "hello", cosmos.NewUint(500), ""), NotNil)
	// set non-existent mayaname as subaffiliate - should fail
	c.Assert(s.setMayanameSimple("hello", EmptyBps, "nobody", cosmos.NewUint(500), ""), NotNil)
	// set out of range affiliate bps - should fail
	c.Assert(s.setMayanameSimple("aff1", EmptyBps, "", cosmos.NewUint(100_01), ""), NotNil)
	// set invalid preferred asset - should fail
	c.Assert(s.setMayanameSimple("aff1", EmptyBps, "", EmptyBps, "BAD.THOR"), NotNil)
}

func (s *MultipleAffiliatesSuite) TestMultipleAffiliatesMemos(c *C) {
	// register mayaname with default bps 500
	c.Assert(s.setMayanameSimple("hello", cosmos.NewUint(500), "", EmptyBps, ""), IsNil)
	hello, err := s.mgr.Keeper().GetMAYAName(s.ctx, "hello")
	c.Assert(err, IsNil)
	c.Assert(hello.Name, Equals, "hello")
	c.Assert(hello.GetAffiliateBps().Equal(cosmos.NewUint(500)), Equals, true)
	// set mayaname default bps 0
	c.Assert(s.setMayanameSimple("hello", cosmos.ZeroUint(), "", EmptyBps, ""), IsNil)
	hello, err = s.mgr.Keeper().GetMAYAName(s.ctx, "hello")
	c.Assert(err, IsNil)
	c.Assert(hello.Name, Equals, "hello")
	c.Assert(hello.GetAffiliateBps().IsZero(), Equals, true)
	// set default bps back t 500
	c.Assert(s.setMayanameSimple("hello", cosmos.NewUint(500), "", EmptyBps, ""), IsNil)
	hello, err = s.mgr.Keeper().GetMAYAName(s.ctx, "hello")
	c.Assert(err, IsNil)
	c.Assert(hello.Name, Equals, "hello")
	c.Assert(hello.GetAffiliateBps().Equal(cosmos.NewUint(500)), Equals, true)
	// register mayaname aff1
	c.Assert(s.setMayanameSimple("aff1", EmptyBps, "", EmptyBps, ""), IsNil)
	// register mayaname aff2
	c.Assert(s.setMayanameSimple("aff2", EmptyBps, "", EmptyBps, ""), IsNil)
	// set aff1 as subaff of hello with 2000 bps share
	c.Assert(s.setMayanameSimple("hello", EmptyBps, "aff1", cosmos.NewUint(2000), ""), IsNil)
	hello, err = s.mgr.Keeper().GetMAYAName(s.ctx, "hello")
	c.Assert(err, IsNil)
	c.Assert(hello.Subaffiliates, HasLen, 1)
	c.Assert(hello.Subaffiliates[0].Name, Equals, "aff1")
	c.Assert(hello.Subaffiliates[0].Bps.Equal(cosmos.NewUint(2000)), Equals, true)
	// set aff2 as subaff of hello with 3000 bps share
	c.Assert(s.setMayanameSimple("hello", EmptyBps, "aff2", cosmos.NewUint(3000), ""), IsNil)
	hello, err = s.mgr.Keeper().GetMAYAName(s.ctx, "hello")
	c.Assert(err, IsNil)
	c.Assert(hello.Subaffiliates, HasLen, 2)
	c.Assert(hello.Subaffiliates[0].Name, Equals, "aff1")
	c.Assert(hello.Subaffiliates[0].Bps.Equal(cosmos.NewUint(2000)), Equals, true)
	c.Assert(hello.Subaffiliates[1].Name, Equals, "aff2")
	c.Assert(hello.Subaffiliates[1].Bps.Equal(cosmos.NewUint(3000)), Equals, true)
	// remove aff1 as subaff of hello
	c.Assert(s.setMayanameSimple("hello", EmptyBps, "aff1", cosmos.ZeroUint(), ""), IsNil)
	hello, err = s.mgr.Keeper().GetMAYAName(s.ctx, "hello")
	c.Assert(err, IsNil)
	c.Assert(hello.Subaffiliates, HasLen, 1)
	c.Assert(hello.Subaffiliates[0].Name, Equals, "aff2")
	c.Assert(hello.Subaffiliates[0].Bps.Equal(cosmos.NewUint(3000)), Equals, true)
	// set aff1 as subaff of hello again with 4000 bps share
	c.Assert(s.setMayanameSimple("hello", EmptyBps, "aff1", cosmos.NewUint(4000), ""), IsNil)
	hello, err = s.mgr.Keeper().GetMAYAName(s.ctx, "hello")
	c.Assert(err, IsNil)
	c.Assert(hello.Subaffiliates, HasLen, 2)
	c.Assert(hello.Subaffiliates[0].Name, Equals, "aff2")
	c.Assert(hello.Subaffiliates[0].Bps.Equal(cosmos.NewUint(3000)), Equals, true)
	c.Assert(hello.Subaffiliates[1].Name, Equals, "aff1")
	c.Assert(hello.Subaffiliates[1].Bps.Equal(cosmos.NewUint(4000)), Equals, true)
}

func (s *MultipleAffiliatesSuite) TestVerySmallAffiliateSwapFees(c *C) {
	ttds := s.getRandomTTHORAddress().String()
	perc100 := cosmos.NewUint(10000)

	// *********************************************************************************
	// 1. swap from cacao to rune via MsgDeposit
	//    the tiny aff fee is moved to the affiliate's maya alias or affiliate collector
	//    in this scenario no tx fee is charged for aff fee move
	// *********************************************************************************

	// prepare the affiliate mayaname
	affBps := cosmos.NewUint(2)
	c.Assert(s.setMayanameSimple("hello", affBps, "", EmptyBps, ""), IsNil)
	hello, err := s.mgr.Keeper().GetMAYAName(s.ctx, "hello")
	c.Assert(err, IsNil)
	c.Assert(hello.Name, Equals, "hello")
	c.Assert(hello.GetAffiliateBps().Equal(cosmos.NewUint(2)), Equals, true)
	helloBaseAliasAcc, _ := hello.GetAlias(common.BASEChain).AccAddress()
	helloBalanceBefore := cosmos.NewUint(s.mgr.Keeper().GetBalance(s.ctx, helloBaseAliasAcc).AmountOf(common.BaseAsset().Native()).Uint64())

	// msgDeposit - swap from acaco to rune
	swapAmt := cosmos.NewUint(100 * common.One) // swap 1 cacao
	coins := common.Coins{
		common.NewCoin(common.BaseNative, swapAmt),
	}
	memo := fmt.Sprintf("=:THOR.RUNE:%s::hello", ttds)
	msg := NewMsgDeposit(coins, memo, s.signer)
	err = s.handleDeposit(msg)
	c.Assert(err, IsNil)

	// verify results
	// preferred asset not set so affiliate fee goes right to the affiliate cacao alias
	affFees := common.GetSafeShare(affBps, perc100, swapAmt)
	helloBalanceAfter := cosmos.NewUint(s.mgr.Keeper().GetBalance(s.ctx, helloBaseAliasAcc).AmountOf(common.BaseAsset().Native()).Uint64())
	c.Check(helloBalanceBefore.Add(affFees).Equal(helloBalanceAfter), Equals, true)
	helloBalanceBefore = helloBalanceAfter
	// verify swap queue
	swaps, err := s.fetchQueue()
	c.Assert(err, IsNil)
	c.Assert(swaps, HasLen, 1)
	expectedSwapAmt := common.SafeSub(swapAmt, affFees)
	c.Check(swaps[0].msg.Tx.Coins[0].Amount.Equal(expectedSwapAmt), Equals, true)
	err = s.queue.EndBlock(s.ctx, s.mgr)
	c.Assert(err, IsNil)
	// verify out tx
	c.Check(s.mockTxOutStore.tois, HasLen, 1)
	c.Check(s.mockTxOutStore.tois[0].ToAddress.String(), Equals, ttds)
	s.clearAllAffiliateCollectors()
	s.mockTxOutStore.tois = nil

	// *********************************************************************************
	// 2. Swap from BNB to RUNE
	//	2.1. swapAmt == 100 * common.One
	//    The tiny affiliate fee is not swapped because the affiliate fee is smaller than
	//    the native cacao transaction fee that swapper.Swap wants to charge for moving the cacao.
	//    First we try to refund the failed aff fee swap. In this example it succeeds because
	//    of skewed ratio of cacao:bnb so the bnb tx refund fee is smaller than the affiliate fee
	//    so the refund is successful.
	//    In this case the main swap is successful and the affiliate fee swap is refunded to the sender :)
	//
	//  2.2. swapAmt == 1 * common.One
	//    In case the refund fails because the refund tx fee is bigger than the affiliate fee,
	//    the following scenario uccurs:
	//    The affiliate fee amount (which is less than the native transaction fee)
	//    is added to the Reserve here using cashed context:
	//    https://gitlab.com/mayachain/mayanode/-/blame/develop/x/mayachain/manager_txout_current.go#L623
	//    but later, in this line, cachedTryAddTxOutItem returns an error:
	//    https://gitlab.com/mayachain/mayanode/-/blame/develop/x/mayachain/manager_txout_current.go#L156
	//    This error causes the context not to be committed here:
	//    https://gitlab.com/mayachain/mayanode/-/blame/develop/x/mayachain/manager_txout_current.go#L140
	//    so the AddPoolFeeToReserve is rollbacked, meaning
	//    the tiny affiliate fee remains in the Asgard vault, and there is no record of it anywhere.
	// *********************************************************************************

	// set tx source to BNB
	gasFee := s.mgr.gasMgr.GetFee(s.ctx, common.BNBChain, common.BNBAsset)
	s.tx.Gas = common.Gas{common.NewCoin(common.BNBAsset, gasFee)}
	s.tx.FromAddress = GetRandomBNBAddress()
	s.tx.Coins = common.Coins{common.NewCoin(common.BNBAsset, swapAmt)}
	// save the BNB pool & reserve module balance
	pool, err := s.mgr.Keeper().GetPool(s.ctx, common.BNBAsset)
	c.Assert(err, IsNil)
	bnbPoolAssetBalanceBefore := pool.BalanceAsset
	reserveBefore := s.mgr.Keeper().GetRuneBalanceOfModule(s.ctx, ReserveName)
	asgardBefore := s.mgr.Keeper().GetRuneBalanceOfModule(s.ctx, AsgardName)

	// observe tx - swap from bnb to rune
	swapAmt = cosmos.NewUint(100 * common.One) // swap 1 bnb, bnb:rune:cacao = 1:1:1
	s.tx.Coins[0].Amount = swapAmt
	s.tx.Memo = fmt.Sprintf("=:THOR.RUNE:%s::hello", ttds)

	// process the tx
	c.Assert(s.processTx(), IsNil)

	// verify swap queue, both the main swap and the affiliate fee swap should be queued
	affFees = common.GetSafeShare(affBps, perc100, swapAmt)
	swaps, err = s.fetchQueue()
	c.Assert(err, IsNil)
	c.Assert(swaps, HasLen, 2)
	expectedSwapAmt = common.SafeSub(swapAmt, affFees)
	c.Check(swaps[0].msg.Tx.Coins[0].Amount.Equal(expectedSwapAmt), Equals, true)
	c.Check(swaps[1].msg.Tx.Coins[0].Amount.Equal(affFees), Equals, true)
	err = s.queue.EndBlock(s.ctx, s.mgr)
	c.Assert(err, IsNil)

	// verify out tx, only the main swap should be here, affiliate swap should have been dropped, because native fee > amount
	// in our case the bnb:cacao pool ratio is 1:1, the bnb outbound fee is less than the cacao native tx fee
	// so the failed affiliate fee swap is successfully refunded and the bnb chain fee ismoved to the bnb pool
	// so the second tx is the affiliate fee swap refund transaction
	c.Check(s.mockTxOutStore.tois, HasLen, 2)
	c.Check(s.mockTxOutStore.tois[0].ToAddress.String(), Equals, ttds)
	c.Check(s.mockTxOutStore.tois[1].ToAddress.String(), Equals, s.tx.FromAddress.String())
	c.Check(strings.Contains(s.mockTxOutStore.tois[1].Memo, "REFUND:"), Equals, true)
	s.mockTxOutStore.tois = nil

	// verify error logs
	eStr, eCnt := s.errLogs.GetCollectedString(true)
	c.Assert(eCnt > 0, Equals, true)
	c.Assert(strings.Contains(eStr, "fail to add outbound tx") && strings.Contains(eStr, "not enough asset to pay for fees"), Equals, true)
	// c.Assert(strings.Contains(eStr, "fail to refund non-native tx, leaving coins in vault"), Equals, true)

	// verify BNB pool asset
	pool, err = s.mgr.Keeper().GetPool(s.ctx, common.BNBAsset)
	transactionFeeBNB := s.mgr.GasMgr().GetFee(s.ctx, common.BNBChain, common.BNBAsset)
	c.Assert(err, IsNil)
	bnbPoolAssetBalanceExpected := bnbPoolAssetBalanceBefore.Add(swapAmt).Sub(affFees).Add(transactionFeeBNB)
	bnbPoolAssetBalanceAfter := pool.BalanceAsset
	c.Assert(bnbPoolAssetBalanceExpected.Equal(bnbPoolAssetBalanceAfter), Equals, true)

	// the thorchain otbound tx fee (4075159 cacao) is moved from asgard to reserve
	asgardAfter := s.mgr.Keeper().GetRuneBalanceOfModule(s.ctx, AsgardName)
	tcOutbFeInCacao := asgardBefore.Sub(asgardAfter)
	c.Assert(tcOutbFeInCacao.Equal(cosmos.NewUint(4075159)), Equals, true)
	// no change in Asgard
	reserveAfter := s.mgr.Keeper().GetRuneBalanceOfModule(s.ctx, ReserveName)
	reserveExpected := reserveBefore.Add(tcOutbFeInCacao)
	c.Assert(reserveExpected.Equal(reserveAfter), Equals, true)

	// **********************************************
	// 3. swap from BNB to CACAO
	//  3.1. swapAmt == 100 * common.One
	//    The tiny affiliate fee is not swapped because the affiliate fee is smaller than
	//    the native cacao transaction fee that swapper.Swap wants to charge for moving the cacao.
	//    First we try to refund the failed aff fee swap. In this example it succeeds because
	//    of skewed ratio of cacao:bnb so the bnb tx refund fee is smaller than the affiliate fee
	//    so the refund is successful.
	//    In this case the main swap is successful and the affiliate fee swap is refunded to the sender :)
	//
	//  3.1. swapAmt == 1 * common.One:
	//    We try to refund the failed aff fee swap, but the refund fails as well
	//    because the refund tx fee (in this case BNB native tx fee)
	//    is bigger than the affiliate fee, and the following scenario occurs:
	//    The affiliate fee amount is added to the Reserve here using cashed context:
	//    https://gitlab.com/mayachain/mayanode/-/blame/develop/x/mayachain/manager_txout_current.go#L623
	//    but later, in this line, cachedTryAddTxOutItem returns an error:
	//    https://gitlab.com/mayachain/mayanode/-/blame/develop/x/mayachain/manager_txout_current.go#L156
	//    This error causes the context not to be committed here:
	//    https://gitlab.com/mayachain/mayanode/-/blame/develop/x/mayachain/manager_txout_current.go#L140
	//    so the AddPoolFeeToReserve is rollbacked, meaning
	//    the tiny affiliate fee remains in the Asgard vault, and there is no record of it anywhere.
	// **********************************************

	ttds = s.getRandomTMAYAAddress().String()
	// outboundTxFee := s.mgr.GetConstants().GetInt64Value(constants.OutboundTransactionFee)

	// save the BNB pool & reserve module balance
	pool, err = s.mgr.Keeper().GetPool(s.ctx, common.BNBAsset)
	c.Assert(err, IsNil)
	bnbPoolAssetBalanceBefore = pool.BalanceAsset

	// observe tx - swap from bnb to cacao
	swapAmt = cosmos.NewUint(100 * common.One) // swap 1 bnb, bnb:cacao = 1:1
	// swapAmt = swapAmt.Add(cosmos.NewUint(uint64(outboundTxFee)))
	s.tx.Coins[0].Amount = swapAmt
	s.tx.Memo = fmt.Sprintf("=:MAYA.CACAO:%s::hello", ttds)

	// process the tx
	c.Assert(s.processTx(), IsNil)

	// verify swap queue, both the main swap and the affiliate fee swap should be queued
	affFees = common.GetSafeShare(affBps, perc100, swapAmt)
	swaps, err = s.fetchQueue()
	c.Assert(err, IsNil)
	c.Assert(swaps, HasLen, 2)
	expectedSwapAmt = common.SafeSub(swapAmt, affFees)
	c.Check(swaps[0].msg.Tx.Coins[0].Amount.Equal(expectedSwapAmt), Equals, true)
	c.Check(swaps[1].msg.Tx.Coins[0].Amount.Equal(affFees), Equals, true)
	err = s.queue.EndBlock(s.ctx, s.mgr)
	c.Assert(err, IsNil)

	// verify error logs
	eStr, eCnt = s.errLogs.GetCollectedString(true)
	c.Assert(eCnt > 0, Equals, true)
	c.Assert(strings.Contains(eStr, "fail to add outbound tx") && strings.Contains(eStr, "not enough asset to pay for fees"), Equals, true)
	// c.Assert(strings.Contains(eStr, "fail to refund non-native tx, leaving coins in vault"), Equals, true)

	// variant 2.: verify out tx, only the main swap should be here, affiliate swap should have been dropped, because native fee > amount

	// verify out tx, only the main swap should be here, affiliate swap should have been dropped, because native fee > amount
	// in our case the bnb:cacao pool ratio is 1:1, the bnb outbound fee is less than the cacao native tx fee
	// so the failed affiliate fee swap is successfully refunded and the bnb chain fee is moved to the bnb pool
	// so the second tx is the affiliate fee swap refund transaction
	c.Check(s.mockTxOutStore.tois, HasLen, 2)
	c.Check(s.mockTxOutStore.tois[0].ToAddress.String(), Equals, ttds)
	c.Check(s.mockTxOutStore.tois[1].ToAddress.String(), Equals, s.tx.FromAddress.String())
	c.Check(strings.Contains(s.mockTxOutStore.tois[1].Memo, "REFUND:"), Equals, true)
	s.mockTxOutStore.tois = nil

	// verify BNB pool asset
	pool, err = s.mgr.Keeper().GetPool(s.ctx, common.BNBAsset)
	transactionFeeBNB = s.mgr.GasMgr().GetFee(s.ctx, common.BNBChain, common.BNBAsset)
	c.Assert(err, IsNil)
	bnbPoolAssetBalanceExpected = bnbPoolAssetBalanceBefore.Add(swapAmt).Sub(affFees).Add(transactionFeeBNB)
	bnbPoolAssetBalanceAfter = pool.BalanceAsset
	c.Assert(bnbPoolAssetBalanceExpected.Equal(bnbPoolAssetBalanceAfter), Equals, true)
}

func (s *MultipleAffiliatesSuite) TestMultipleAffiliatesWithStreamingSwaps(c *C) {
	destination := s.getRandomTTHORAddress().String()
	perc100 := cosmos.NewUint(10000)
	// swap memo format: =:ASSET:DESTADDR:0/1/10:AFFILIATE_ADDR_OR_MAYANAME:FEE:DEXAggregatorAddr:FinalTokenAddr:MinAmountOut
	// prepare the affiliate mayaname
	c.Assert(s.setMayanameSimple("hello", cosmos.NewUint(150), "", EmptyBps, ""), IsNil)
	hello, err := s.mgr.Keeper().GetMAYAName(s.ctx, "hello")
	c.Assert(err, IsNil)
	hello.PreferredAsset = common.EmptyAsset
	s.mgr.Keeper().SetMAYAName(s.ctx, hello)
	addrHelloOnMaya := hello.GetAlias(common.BASEChain)
	accHelloOnMaya, _ := addrHelloOnMaya.AccAddress()
	prevBalance := cosmos.NewUint(s.mgr.Keeper().GetBalance(s.ctx, accHelloOnMaya).AmountOf(common.BaseAsset().Native()).Uint64())
	swapAmt := cosmos.NewUint(1000000 * common.One)
	coins := common.Coins{
		common.NewCoin(common.BaseNative, swapAmt),
	}
	coins[0].Amount = swapAmt
	memo := "=:THOR.RUNE:" + destination + ":0/1/10:hello"
	msg := NewMsgDeposit(coins, memo, s.signer)
	err = s.handleDeposit(msg)
	c.Assert(err, IsNil)
	affCol, err := s.mgr.Keeper().GetAffiliateCollector(s.ctx, hello.Owner)
	c.Assert(err, IsNil)
	c.Check(affCol.CacaoAmount.IsZero(), Equals, true)
	// verify swap queue
	swaps, err := s.fetchQueue()
	c.Assert(err, IsNil)
	c.Check(swaps, HasLen, 1)
	err = s.queue.EndBlock(s.ctx, s.mgr)
	c.Assert(err, IsNil)
	affFees := common.GetSafeShare(cosmos.NewUint(150), perc100, swapAmt)
	// verify that affiliate fees have been sent to the preferred asset alias on native chain
	curBalance := cosmos.NewUint(s.mgr.Keeper().GetBalance(s.ctx, accHelloOnMaya).AmountOf(common.BaseAsset().Native()).Uint64())
	c.Assert(prevBalance.Add(affFees).Equal(curBalance), Equals, true)
}

func (s *MultipleAffiliatesSuite) TestSwapFromCacaoAndBtcToEthWithPeerAndNestedAff(c *C) {
	version := GetCurrentVersion()
	affCol, err := s.mgr.Keeper().GetModuleAddress(AffiliateCollectorName)
	c.Assert(err, IsNil)
	accAffCol, err := affCol.AccAddress()
	c.Assert(err, IsNil)

	coins := common.Coins{common.NewCoin(common.BaseNative, cosmos.NewUint(3_000*common.One))}
	cat := GetRandomBaseAddress()
	fox := GetRandomBaseAddress()
	pig := GetRandomBaseAddress()
	frog := GetRandomBaseAddress()
	bat := GetRandomBaseAddress()
	bee := GetRandomBaseAddress()

	owlOwner := GetRandomBaseAddress()
	antOwner := GetRandomBaseAddress()

	catEth := GetRandomETHAddress()
	foxEth := GetRandomETHAddress()
	owlEth := GetRandomETHAddress()
	destEth := GetRandomETHAddress()

	antBtc := GetRandomBTCAddress()

	// prepare mayanames
	// memo format:  ~:name:chain:address:?owner:?preferredAsset:?expiry:?affbps:?subaff:?subaffbps
	// bat
	msg := NewMsgDeposit(coins, fmt.Sprintf("~:bat:MAYA:%s", bat), s.signer)
	c.Assert(s.handleDeposit(msg), IsNil)
	// owl
	msg = NewMsgDeposit(coins, fmt.Sprintf("~:owl:ETH:%s:%s", owlEth, owlOwner), s.signer)
	c.Assert(s.handleDeposit(msg), IsNil)
	// fox
	msg = NewMsgDeposit(coins, fmt.Sprintf("~:fox:MAYA:%s", fox), s.signer)
	c.Assert(s.handleDeposit(msg), IsNil)
	// pig
	msg = NewMsgDeposit(coins, fmt.Sprintf("~:pig:MAYA:%s", pig), s.signer)
	c.Assert(s.handleDeposit(msg), IsNil)
	// frog
	msg = NewMsgDeposit(coins, fmt.Sprintf("~:frog:MAYA:%s::::100:bat:4000", frog), s.signer)
	c.Assert(s.handleDeposit(msg), IsNil)
	// cat with fox
	msg = NewMsgDeposit(coins, fmt.Sprintf("~:cat:MAYA:%s::::50:fox:1000", cat), s.signer)
	c.Assert(s.handleDeposit(msg), IsNil)
	// cat with frog
	msg = NewMsgDeposit(coins, "~:cat:::::::frog:2000", s.signer)
	c.Assert(s.handleDeposit(msg), IsNil)
	// pig with owl
	msg = NewMsgDeposit(coins, "~:pig:::::::owl:5000", s.signer)
	c.Assert(s.handleDeposit(msg), IsNil)
	// fox's pref asseet is ETH
	msg = NewMsgDeposit(coins, fmt.Sprintf("~:fox:ETH:%s::ETH.ETH", foxEth), s.signer)
	c.Assert(s.handleDeposit(msg), IsNil)
	// cat's pref asset is ETH
	msg = NewMsgDeposit(coins, fmt.Sprintf("~:cat:ETH:%s::ETH.ETH", catEth), s.signer)
	c.Assert(s.handleDeposit(msg), IsNil)
	// ant
	msg = NewMsgDeposit(coins, fmt.Sprintf("~:ant:BTC:%s:%s:::50", antBtc, antOwner), s.signer)
	c.Assert(s.handleDeposit(msg), IsNil)

	swapAmt := cosmos.NewUint(10000_00000000)
	s.tx.Coins = common.Coins{common.NewCoin(common.BTCAsset, swapAmt)}
	// cat (0.5%, eth)
	//      - fox  (10%, eth)
	//      - frog (20%)
	//           - bat (40%)
	// pig (-%)
	//      - owl (50%, no maya alias only eth)
	// ant (0.5%, no maya alias only btc)
	expextedRes := []struct {
		bps  uint64
		addr common.Address
	}{
		{9810, destEth}, // 10000 - 190
		{5, affCol},     // fox 5  (10% of 50)
		{4, bat},        // bat 4  (40% of 20% of 50)
		{6, frog},       // frog 6 (60% of 20% of 50)
		{35, affCol},    // cat 35 (70% of 50 = (50 - 30%))
		{40, bee},       // bee 40
		{25, owlOwner},  // owl 25 (50% of 50)
		{25, pig},       // pig 25 (50% of 50)
		{50, antOwner},  // ant 50
	}

	// save the current balances of maya addresses
	prevBalances := make([]cosmos.Uint, len(expextedRes))
	for i, res := range expextedRes {
		if res.addr.IsChain(common.BASEChain, version) && !res.addr.Equals(affCol) {
			acc, _ := res.addr.AccAddress()
			prevBalances[i] = cosmos.NewUint(s.mgr.Keeper().GetBalance(s.ctx, acc).AmountOf(common.BaseAsset().Native()).Uint64())
		}
	}
	prevAffColBalance := cosmos.NewUint(s.mgr.Keeper().GetBalance(s.ctx, accAffCol).AmountOf(common.BaseAsset().Native()).Uint64())

	// swap using combined peer affiliates & subaffiliates tree
	memo := fmt.Sprintf("=:ETH.ETH:%s:0/1:cat/%s/pig/ant:/40/50/", destEth, bee) // pig has no defbps

	// *** swap from CACAO (MsgDeposit)
	swapCoins := common.Coins{common.NewCoin(common.BaseNative, swapAmt)}
	msg = NewMsgDeposit(swapCoins, memo, s.signer)
	c.Assert(s.handleDeposit(msg), IsNil)
	// verify swap queue - only the main swap should be queued
	swaps, err := s.fetchQueue()
	c.Assert(err, IsNil)
	c.Assert(swaps, HasLen, 1)
	// verify the main swap destination and amount
	c.Check(swaps[0].msg.Destination.Equals(expextedRes[0].addr), Equals, true)
	exp := swapAmt.MulUint64(expextedRes[0].bps).QuoUint64(10000).Uint64()
	got := swaps[0].msg.Tx.Coins[0].Amount.Uint64()
	c.Check(exp, Equals, got)
	// verify fees' destinations and amounts
	feesAddedToffCol := cosmos.ZeroUint()
	for i, res := range expextedRes {
		if res.addr.IsChain(common.BASEChain, version) {
			fee := swapAmt.MulUint64(res.bps).QuoUint64(10000).Uint64()
			if res.addr.Equals(affCol) {
				feesAddedToffCol = feesAddedToffCol.AddUint64(fee)
			} else {
				acc, _ := res.addr.AccAddress()
				curBalance := cosmos.NewUint(s.mgr.Keeper().GetBalance(s.ctx, acc).AmountOf(common.BaseAsset().Native()).Uint64())
				exp = prevBalances[i].AddUint64(fee).Uint64()
				got = curBalance.Uint64()
				c.Check(exp, Equals, got)
			}
		}
	}
	// verify the affiliate collector amount
	curAffColBalance := cosmos.NewUint(s.mgr.Keeper().GetBalance(s.ctx, accAffCol).AmountOf(common.BaseAsset().Native()).Uint64())
	exp = prevAffColBalance.Add(feesAddedToffCol).Uint64()
	got = curAffColBalance.Uint64()
	c.Check(exp, Equals, got)
	// check tx outs
	c.Assert(s.queue.EndBlock(s.ctx, s.mgr), IsNil)
	c.Check(s.mockTxOutStore.tois, HasLen, 1)
	s.errLogs.Clear()

	// *** swap from BTC
	// set tx source to BTC
	fromBtc := GetRandomBTCAddress()
	gasFee := s.mgr.gasMgr.GetFee(s.ctx, common.BTCChain, common.BTCAsset)
	s.tx.Gas = common.Gas{common.NewCoin(common.BTCAsset, gasFee)}
	s.tx.FromAddress = fromBtc
	swapAmt = cosmos.NewUint(10000_00000000)

	// process the tx
	s.tx.Memo = memo
	err = s.processTx()
	c.Assert(err, IsNil)
	// verify swap queue
	swaps, err = s.fetchQueue()
	c.Assert(err, IsNil)
	c.Assert(swaps, HasLen, 9)
	// verify swap destinations and amounts
	for i, res := range expextedRes {
		c.Check(swaps[i].msg.Destination.Equals(res.addr), Equals, true)
		exp := swapAmt.MulUint64(res.bps).QuoUint64(10000).Uint64()
		got := swaps[i].msg.Tx.Coins[0].Amount.Uint64()
		c.Check(exp, Equals, got)
	}
	// check tx outs
	c.Assert(s.queue.EndBlock(s.ctx, s.mgr), IsNil)
	c.Check(s.mockTxOutStore.tois, HasLen, 9)
	s.mockTxOutStore.tois = nil
	s.errLogs.Clear()
	_ = s.queue.EndBlock(s.ctx, s.mgr)
	s.errLogs.Clear()

	// **
	// swap with only one affiliate bps that is used in all affiliates except mayanameswith default bps set
	swapAmt = cosmos.NewUint(10000_00000000)
	s.tx.Coins = common.Coins{common.NewCoin(common.BTCAsset, swapAmt)}

	expextedRes = []struct {
		bps  uint64
		addr common.Address
	}{
		// multiply basis points by 10 to shift decimal for further processing to increase precision for calculations
		{98900, destEth}, // 10000 - 110 = 9890 (20 + 20 + 20 + 50)
		{20, affCol},     // fox 2  (10% of 20)
		{16, bat},        // bat 1.6  (40% of 20% of 50)
		{24, frog},       // frog 2.4 (60% of 20% of 20)
		{140, affCol},    // cat 14  (70% of 20)
		{200, bee},       // bee 20
		{100, owlOwner},  // owl 10 (50% of 20)
		{100, pig},       // pig 10 (50% of 20)
		{500, antOwner},  // ant 50
	}
	// cat - 20, bee - 20, pig - 20, ant - 50
	s.tx.Memo = fmt.Sprintf("=:ETH.ETH:%s:0/1:cat/%s/pig/ant:20/20/20/", destEth, bee) // pig has no defbps
	// process the tx
	err = s.processTx()
	c.Assert(err, IsNil)
	// verify swap queue
	swaps, err = s.fetchQueue()
	c.Assert(err, IsNil)
	c.Assert(swaps, HasLen, 9)
	// the main swap
	for i, res := range expextedRes {
		// the main swap
		c.Check(swaps[i].msg.Destination.Equals(res.addr), Equals, true)
		// adjust by dividing the result by 100000 to compensate for the earlier scaling
		exp := swapAmt.MulUint64(res.bps).QuoUint64(100000).Uint64()
		got := swaps[i].msg.Tx.Coins[0].Amount.Uint64()
		c.Check(exp, Equals, got)
	}
	s.mockTxOutStore.tois = nil
	_ = s.queue.EndBlock(s.ctx, s.mgr)
	s.errLogs.Clear()

	// cat (0.5%, eth)
	//      - fox  (10%, eth)
	//      - frog (20%)
	//           - bat (40%)
	// pig (-%)
	//      - owl (50%, no maya alias only eth)
	// ant (0.5%, no maya alias only btc)
	s.tx.Coins = common.Coins{common.NewCoin(common.BTCAsset, swapAmt)}
	expextedRes = []struct {
		bps  uint64
		addr common.Address
	}{
		// multiply basis points by 10 to shift decimal for further processing to increase precision for calculations
		{99100, destEth}, // 10000 - 90 = 9910 (20 + 20 + 50)
		{20, affCol},     // fox 2  (10% of 20)
		{16, bat},        // bat 1.6  (40% of 20% of 50)
		{24, frog},       // frog 2.4 (60% of 20% of 20)
		{140, affCol},    // cat 14  (70% of 20)
		{200, bee},       // bee 20
		{500, antOwner},  // ant 50
	}
	// cat - 20, bee - 20, pig - 0, ant - 50
	s.tx.Memo = fmt.Sprintf("=:ETH.ETH:%s:0/1:cat/%s/pig/ant:20", destEth, bee) // pig has no defbps
	// process the tx
	err = s.processTx()
	c.Assert(err, IsNil)
	// verify swap queue
	swaps, err = s.fetchQueue()
	c.Assert(err, IsNil)
	c.Assert(swaps, HasLen, 7)
	// the main swap
	for i, res := range expextedRes {
		// the main swap
		c.Check(swaps[i].msg.Destination.Equals(res.addr), Equals, true)
		// adjust by dividing the result by 100000 to compensate for the earlier scaling
		exp := swapAmt.MulUint64(res.bps).QuoUint64(100000).Uint64()
		got := swaps[i].msg.Tx.Coins[0].Amount.Uint64()
		c.Check(exp, Equals, got)
	}
	s.mockTxOutStore.tois = nil
	_ = s.queue.EndBlock(s.ctx, s.mgr)
	s.errLogs.Clear()

	// unhappy paths
	// affiliates & bps count mismatch
	s.tx.Memo = fmt.Sprintf("=:ETH.ETH:%s:0/1:cat/%s/pig/ant:/40/50", destEth, bee)
	err = s.processTx()
	c.Assert(err, NotNil)
	c.Assert(strings.Contains(err.Error(), "affiliate mayanames and affiliate fee bps count mismatch"), Equals, true)
	s.errLogs.Clear()

	// affiliate bps too high
	s.tx.Memo = fmt.Sprintf("=:ETH.ETH:%s:0/1:cat/%s/pig/ant:/500/500/", destEth, bee)
	err = s.processTx()
	c.Assert(err, NotNil)
	c.Assert(strings.Contains(err.Error(), "affiliate fee basis points must not exceed"), Equals, true)
	s.errLogs.Clear()

	// invalid address / mayaname
	s.tx.Memo = fmt.Sprintf("=:ETH.ETH:%s:0/1:cat/%s/pig/ant:/40/50/", destEth, "123")
	err = s.processTx()
	c.Assert(err, NotNil)
	c.Assert(strings.Contains(err.Error(), "invalid affiliate mayaname or address: 123 is not recognizable"), Equals, true)
	s.errLogs.Clear()

	// bee (=explicit addr) doesn't have aff bps
	s.tx.Memo = fmt.Sprintf("=:ETH.ETH:%s:0/1:cat/%s/pig/ant:10///", destEth, bee)
	err = s.processTx()
	c.Assert(strings.Contains(err.Error(), fmt.Sprintf("cannot parse '%s' as a MAYAName while empty affiliate basis points provided at index", bee)), Equals, true)
	c.Assert(err, NotNil)
	s.errLogs.Clear()
}

func (s *MultipleAffiliatesSuite) TestMultipleAffWithZeroBps(c *C) {
	ttds := s.getRandomTTHORAddress().String()
	perc100 := cosmos.NewUint(10000)
	// prepare the affiliate mayaname
	affBps := cosmos.NewUint(150)
	c.Assert(s.setMayanameSimple("hello", cosmos.NewUint(150), "", EmptyBps, ""), IsNil)
	hello, err := s.mgr.Keeper().GetMAYAName(s.ctx, "hello")
	c.Assert(err, IsNil)
	c.Assert(hello.Name, Equals, "hello")
	c.Assert(hello.GetAffiliateBps().Equal(cosmos.NewUint(150)), Equals, true)
	c.Assert(s.setMayanameSimple("helloZero", cosmos.ZeroUint(), "", EmptyBps, ""), IsNil)
	helloZero, err := s.mgr.Keeper().GetMAYAName(s.ctx, "helloZero")
	c.Assert(err, IsNil)
	c.Assert(helloZero.Name, Equals, "helloZero")
	c.Assert(helloZero.GetAffiliateBps().Equal(cosmos.NewUint(0)), Equals, true)
	helloBaseAliasAcc, _ := hello.GetAlias(common.BASEChain).AccAddress()
	helloBalanceBefore := cosmos.NewUint(s.mgr.Keeper().GetBalance(s.ctx, helloBaseAliasAcc).AmountOf(common.BaseAsset().Native()).Uint64())
	helloZeroBaseAliasAcc, _ := helloZero.GetAlias(common.BASEChain).AccAddress()
	helloZeroBalanceBefore := cosmos.NewUint(s.mgr.Keeper().GetBalance(s.ctx, helloZeroBaseAliasAcc).AmountOf(common.BaseAsset().Native()).Uint64())

	// swap with two affiliates, first with 150 bps second with zero bps
	swapAmt := cosmos.NewUint(100000 * common.One) // swap 10 cacao
	coins := common.Coins{common.NewCoin(common.BaseNative, swapAmt)}
	memo := fmt.Sprintf("=:THOR.RUNE:%s::hello/helloZero", ttds)
	msg := NewMsgDeposit(coins, memo, s.signer)
	c.Assert(s.handleDeposit(msg), IsNil)
	affFees := common.GetSafeShare(affBps, perc100, swapAmt)
	helloBalanceAfter := cosmos.NewUint(s.mgr.Keeper().GetBalance(s.ctx, helloBaseAliasAcc).AmountOf(common.BaseAsset().Native()).Uint64())
	c.Check(helloBalanceBefore.Add(affFees).Equal(helloBalanceAfter), Equals, true)
	helloZeroBalanceAfter := cosmos.NewUint(s.mgr.Keeper().GetBalance(s.ctx, helloZeroBaseAliasAcc).AmountOf(common.BaseAsset().Native()).Uint64())
	c.Check(helloZeroBalanceBefore.Equal(helloZeroBalanceAfter), Equals, true)
	helloBalanceBefore = helloBalanceAfter

	// the same swap as above but with overridden bps (50)
	// only one bps provided so it will be used for first affiliates, second uses default mayaname bps, which is zero
	coins = common.Coins{common.NewCoin(common.BaseNative, swapAmt)}
	affBps = cosmos.NewUint(50)
	memo = fmt.Sprintf("=:THOR.RUNE:%s::hello/helloZero:50", ttds)
	msg = NewMsgDeposit(coins, memo, s.signer)
	c.Assert(s.handleDeposit(msg), IsNil)
	affFees = common.GetSafeShare(affBps, perc100, swapAmt)
	helloBalanceAfter = cosmos.NewUint(s.mgr.Keeper().GetBalance(s.ctx, helloBaseAliasAcc).AmountOf(common.BaseAsset().Native()).Uint64())
	c.Check(helloBalanceBefore.Add(affFees).Equal(helloBalanceAfter), Equals, true)
	helloZeroBalanceAfter = cosmos.NewUint(s.mgr.Keeper().GetBalance(s.ctx, helloZeroBaseAliasAcc).AmountOf(common.BaseAsset().Native()).Uint64())
	c.Check(helloZeroBalanceBefore.Equal(helloZeroBalanceAfter), Equals, true)
	helloBalanceBefore = helloBalanceAfter

	// the same swap as above but with overridden bps (50) only for first affiliate
	// first affiliate uses 50, second uses default mayaname bps, which is zero
	coins = common.Coins{common.NewCoin(common.BaseNative, swapAmt)}
	affBps = cosmos.NewUint(50)
	memo = fmt.Sprintf("=:THOR.RUNE:%s::hello/helloZero:50/", ttds)
	msg = NewMsgDeposit(coins, memo, s.signer)
	c.Assert(s.handleDeposit(msg), IsNil)
	affFees = common.GetSafeShare(affBps, perc100, swapAmt)
	helloBalanceAfter = cosmos.NewUint(s.mgr.Keeper().GetBalance(s.ctx, helloBaseAliasAcc).AmountOf(common.BaseAsset().Native()).Uint64())
	c.Check(helloBalanceBefore.Add(affFees).Equal(helloBalanceAfter), Equals, true)
	helloZeroBalanceAfter = cosmos.NewUint(s.mgr.Keeper().GetBalance(s.ctx, helloZeroBaseAliasAcc).AmountOf(common.BaseAsset().Native()).Uint64())
	c.Check(helloZeroBalanceBefore.Equal(helloZeroBalanceAfter), Equals, true)
	helloBalanceBefore = helloBalanceAfter

	// the same swap as above but with overridden bps (50) explicitly for both affiliate
	// first affiliate uses 50, second uses 50
	coins = common.Coins{common.NewCoin(common.BaseNative, swapAmt)}
	memo = fmt.Sprintf("=:THOR.RUNE:%s::hello/helloZero:50/50", ttds)
	msg = NewMsgDeposit(coins, memo, s.signer)
	c.Assert(s.handleDeposit(msg), IsNil)
	affFees = common.GetSafeShare(affBps, perc100, swapAmt)
	helloBalanceAfter = cosmos.NewUint(s.mgr.Keeper().GetBalance(s.ctx, helloBaseAliasAcc).AmountOf(common.BaseAsset().Native()).Uint64())
	c.Check(helloBalanceBefore.Add(affFees).Equal(helloBalanceAfter), Equals, true)
	helloZeroBalanceAfter = cosmos.NewUint(s.mgr.Keeper().GetBalance(s.ctx, helloZeroBaseAliasAcc).AmountOf(common.BaseAsset().Native()).Uint64())
	c.Check(helloZeroBalanceBefore.Add(affFees).Equal(helloZeroBalanceAfter), Equals, true)
}

func (s *MultipleAffiliatesSuite) TestSubaffiliateBps(c *C) {
	coins := common.Coins{common.NewCoin(common.BaseNative, cosmos.NewUint(3_000*common.One))}
	cat := GetRandomBaseAddress()
	fox := GetRandomBaseAddress()
	bee := GetRandomBaseAddress()

	// prepare mayanames
	// memo format:  ~:name:chain:address:?owner:?preferredAsset:?expiry:?affbps:?subaff:?subaffbps
	msg := NewMsgDeposit(coins, fmt.Sprintf("~:cat:MAYA:%s", cat), s.signer)
	c.Assert(s.handleDeposit(msg), IsNil)
	msg = NewMsgDeposit(coins, fmt.Sprintf("~:fox:MAYA:%s", fox), s.signer)
	c.Assert(s.handleDeposit(msg), IsNil)

	// test common subaffiliate bps
	// set address and fox with common sub-affiliate bps 2000
	msg = NewMsgDeposit(coins, fmt.Sprintf("~:cat:::::::%s/fox:2000", bee), s.signer)
	c.Assert(s.handleDeposit(msg), IsNil)
	catMn, err := s.mgr.Keeper().GetMAYAName(s.ctx, "cat")
	c.Assert(err, IsNil)
	c.Assert(catMn.Subaffiliates, HasLen, 2)
	c.Assert(catMn.Subaffiliates[0].Name, Equals, bee.String())
	c.Assert(catMn.Subaffiliates[0].Bps.BigInt().Uint64(), Equals, uint64(2000))
	c.Assert(catMn.Subaffiliates[1].Name, Equals, "fox")
	c.Assert(catMn.Subaffiliates[1].Bps.BigInt().Uint64(), Equals, uint64(2000))

	// test change and add subaffiliate at the same time
	owl := GetRandomBaseAddress()
	msg = NewMsgDeposit(coins, fmt.Sprintf("~:cat:::::::%s/fox:7000/10", owl), s.signer)
	c.Assert(s.handleDeposit(msg), IsNil)
	catMn, err = s.mgr.Keeper().GetMAYAName(s.ctx, "cat")
	c.Assert(err, IsNil)
	c.Assert(catMn.Subaffiliates, HasLen, 3)
	c.Assert(catMn.Subaffiliates[0].Name, Equals, bee.String())
	c.Assert(catMn.Subaffiliates[0].Bps.BigInt().Uint64(), Equals, uint64(2000))
	c.Assert(catMn.Subaffiliates[1].Name, Equals, "fox")
	c.Assert(catMn.Subaffiliates[1].Bps.BigInt().Uint64(), Equals, uint64(10))
	c.Assert(catMn.Subaffiliates[2].Name, Equals, owl.String())
	c.Assert(catMn.Subaffiliates[2].Bps.BigInt().Uint64(), Equals, uint64(7000))

	// test first empty subaff bps
	msg = NewMsgDeposit(coins, fmt.Sprintf("~:cat:::::::%s/fox:/10", owl), s.signer)
	c.Assert(s.handleDeposit(msg), NotNil)

	// test empty subaff bps
	msg = NewMsgDeposit(coins, fmt.Sprintf("~:cat:::::::%s/fox:/", owl), s.signer)
	c.Assert(s.handleDeposit(msg), NotNil)

	// test only one subaff bps with zero value - should delete both subaffiliates
	msg = NewMsgDeposit(coins, fmt.Sprintf("~:cat:::::::%s/fox:0", owl), s.signer)
	c.Assert(s.handleDeposit(msg), IsNil)
	catMn, err = s.mgr.Keeper().GetMAYAName(s.ctx, "cat")
	c.Assert(err, IsNil)
	c.Assert(catMn.Subaffiliates, HasLen, 1)
	c.Assert(catMn.Subaffiliates[0].Name, Equals, bee.String())
	c.Assert(catMn.Subaffiliates[0].Bps.BigInt().Uint64(), Equals, uint64(2000))

	// test first empty subaff bps
	msg = NewMsgDeposit(coins, fmt.Sprintf("~:cat:::::::%s/fox:/10", owl), s.signer)
	c.Assert(s.handleDeposit(msg), NotNil)
}
